<?php
use LineMetrics\LM3\RequestTypes\UpdateObjectRequest;
use LineMetrics\LM3\RequestTypes\AssetRequest;
use LineMetrics\LM3\RequestTypes\ObjectRequest;
use LineMetrics\LM3\Curl\Auth\ClientCredentialsGrant;
use LineMetrics\LM3\Curl\Connector\CurlConnector;

use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use LineMetrics\LM3\DataTypes\DataTypesFactory;
require_once 'vendor/autoload.php';



$log = new Logger('ConnectorCurl');
$log->pushHandler(new StreamHandler('./log/lm3.connector.log', Logger::DEBUG));

$connector = new CurlConnector();
$connector->setLogger($log);

$log = new Logger('AuthClientCredentials');
$log->pushHandler(new StreamHandler('./log/lm3.auth.log', Logger::DEBUG));

$auth = new ClientCredentialsGrant('api_1882521f2ec', '357715ab5d0073c2d1343bb0f906857e');
$auth->setLogger($log);

$api = new LineMetrics\LM3\Api(
    $auth,
    $connector,
    null,
    new DataTypesFactory()
);

$log = new Logger('Api');
$log->pushHandler(new StreamHandler('./log/lm3.api.log', Logger::DEBUG));
$api->setLogger($log);



/*
 * $assets = $api->loadAssets();
 *
 *
 * foreach ( $assets as $asset){
 *
 * echo "Asset '" . $asset->getTitle() . "'\n";
 * echo "ID '" . $asset->getId() . "'\n";
 *
 * foreach( $asset->loadAssets() as $child ){
 * echo "\tAsset '" . $child->getTitle() . "'\n";
 * echo "\tID '" . $child->getId() . "'\n";
 * echo "\n";
 *
 * foreach( $child->loadProperties() as $prop){
 * echo "\t\tProp '" . $prop->getTitle() . "'\n";
 * echo "\t\tType '" . $prop->getPropertyType() . "'\n";
 * echo "\n";
 * }
 * }
 *
 * foreach( $asset->loadProperties() as $prop){
 * echo "\t\tProp '" . $prop->getTitle() . "'\n";
 * echo "\t\tType '" . $prop->getPropertyType() . "'\n";
 * echo "\t\tValue '" . $prop->getValue() . "'\n";
 * echo "\n";
 * }
 *
 * }
 *
 *
 * var_dump( $api->updateObject( new UpdateObjectRequest('TEST-OBJEKTA', array(
 * 'name' => 'UPDATED-TEST-OBJEKTA'
 * ))));
 */

/*
 * $asset = $api->load( new ObjectRequest( 'TEST-OBJEKTA'));
 * //$asset = $api->load( new ObjectRequest( 'METER-DE0000801335700000000000011081296'));
 * $stream = $asset
 * ->loadDataStreams()
 * ->first();
 * //->loadData( strtotime('-1 hour') * 1000, time() * 1000, "Europe/Vienna", "PT5M" );
 * //->loadLastValue();
 *
 *
 * var_dump( $stream->loadLastValue() );
 */


//$prop = $api->load(new ObjectRequest('3035294796f14edb982d0a41fb10e817/meterType'));
//$prop = $api->load(new ObjectRequest('3035294796f14edb982d0a41fb10e817/createdAt'));
//$config = $prop->loadConfig();

//$prop->setValue( $prop->getDataTypesFactory()->createSimpleDate(time()), $config );

//$templates = $api->loadTemplates();
/*
var_dump( $templates );

foreach( $templates as $t ){
    echo $t->getName() . "\n";
    echo $t->getUid(). "\n";
}
*/

/*
$t = $templates->findOneByUid("15c2b313519a439f878d144db8f1e41a");

var_dump($t->loadRequiredFields()->findOneByAlias("meterId"));
exit;

foreach( $templates as $t ){
    var_dump($t->loadRequiredFields()->first());
}
*/

$obj = $api->load(new ObjectRequest('3035294796f14edb982d0a41fb10e817/meterType'));
//$obj = $api->load( new ObjectRequest( 'TEST-OBJEKTA'));
var_dump($obj);
