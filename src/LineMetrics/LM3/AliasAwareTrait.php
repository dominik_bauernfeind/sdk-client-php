<?php
namespace LineMetrics\LM3;

trait AliasAwareTrait
{
    protected $alias;

    public function setAlias($alias)
    {
        $this->alias = $alias;
        return $this;
    }


    public function getAlias()
    {
        return $this->alias;
    }
}
