<?php
namespace LineMetrics\LM3;

interface UidAwareInterface
{
    /**
     * Sets uid
     *
     * @param string|null $uid
     * @return UidAwareInterface
     */
    public function setUid($uid);

    /**
     * Gets uid
     *
     * @return string|null
     */
    public function getUid();
}