<?php
namespace LineMetrics\LM3\DataTypes;

interface DoubleTypeInterface extends BaseTypeInterface
{
    /**
     * Sets double value
     *
     * @param  double $value
     */
    public function setValue($value);

    /**
     * Gets string value
     *
     * @return double
     */
    public function getValue();
}