<?php
namespace LineMetrics\LM3\DataTypes;

interface ConfigurationInterface
{
    /**
     * Gets intput type
     *
     * @return string
     */
    public function getInput();

    /**
     * Gets output type
     *
     * @return string
     */
    public function getOutput();
}