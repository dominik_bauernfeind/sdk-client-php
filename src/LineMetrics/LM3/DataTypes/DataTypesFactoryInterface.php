<?php
namespace LineMetrics\LM3\DataTypes;

interface DataTypesFactoryInterface
{

    /**
     * Gets class name for type name
     *
     * @see DataTypesEnum
     *
     * @param string $typeName
     *            The type name
     * @return string
     */
    public function getClassNameFor($typeName);

    /**
     * Gets interface name for type name
     *
     * @see DataTypesEnum
     *
     * @param string $typeName
     *            The type name
     * @return string
     */
    public function getInterfaceNameFor($typeName);

    /**
     * Creates new boolean type
     *
     * @param boolean $value
     * @param integer $timestamp
     * @return \LineMetrics\LM3\DataTypes\BooleanTypeInterface
     */
    public function createBoolean($value, $timestamp = null);

    /**
     * Creates new double type
     *
     * @param double $value
     * @param integer $timestamp
     * @return \LineMetrics\LM3\DataTypes\DoubleTypeInterface
     */
    public function createDouble($value, $timestamp = null);

    /**
     * Creates new double average type
     *
     * @param double $value
     * @param double $min
     * @param double $max
     * @param integer $timestamp
     * @return \LineMetrics\LM3\DataTypes\DoubleAverageTypeInterface
     */
    public function createDoubleAverage($value, $min, $max, $timestamp = null);

    /**
     * Creates new geo coord type
     *
     * @param double $lat
     * @param double $long
     * @param integer $timestamp
     * @return \LineMetrics\LM3\DataTypes\GeoCoordTypeInterface
     */
    public function createGeooCoord($lat, $long, $timestamp = null);

    /**
     * Creates new geo address type
     *
     * @param string $address
     * @param double $lat
     * @param double $long
     * @param integer $timestamp
     * @return \LineMetrics\LM3\DataTypes\GeoAddressTypeInterface
     */
    public function createGeooAddress($address, $lat, $long, $timestamp = null);

    /**
     * Creates new simple date type
     *
     * @param integer $value
     * @param integer $timestamp
     * @return \LineMetrics\LM3\DataTypes\SimpleDateTypeInterface
     */
    public function createSimpleDate($value, $timestamp = null);

    /**
     * Creates new string type
     *
     * @param string $value
     * @param integer $timestamp
     * @return \LineMetrics\LM3\DataTypes\StringTypeInterface
     */
    public function createString($value, $timestamp = null);

    /**
     * Creates new table type
     *
     * @param array $value
     * @return \LineMetrics\LM3\DataTypes\TableTypeInterface
     */
    public function createTable($value);
}