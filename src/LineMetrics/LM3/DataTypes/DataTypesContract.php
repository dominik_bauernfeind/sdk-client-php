<?php
namespace LineMetrics\LM3\DataTypes;

use LineMetrics\LM3\Collection\ContractInterface;

class DataTypesContract implements ContractInterface
{

    public function loadItem($value)
    {
        return $value;
    }

    public function isValidItem($value)
    {
        return $value instanceof \stdClass;
    }
}