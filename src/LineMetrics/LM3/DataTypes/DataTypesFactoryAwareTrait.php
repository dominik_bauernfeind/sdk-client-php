<?php
namespace LineMetrics\LM3\DataTypes;

trait DataTypesFactoryAwareTrait
{
    /**
     * @var DataTypesFactoryInterface
     */
    protected $typesFactory;



    public function setDataTypesFactory(DataTypesFactoryInterface $factory )
    {
        $this->typesFactory = $factory;
        return $this;
    }
}