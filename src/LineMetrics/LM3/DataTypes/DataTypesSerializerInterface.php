<?php
namespace LineMetrics\LM3\DataTypes;

interface DataTypesSerializerInterface
{
    /**
     * Creates a data type instance from deserialized JSON
      *
     * @param stdClass $payload
     * @return BaseTypeInterface
     */
    public function unserialize(  \stdClass $payload );
}