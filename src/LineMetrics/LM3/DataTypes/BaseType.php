<?php
namespace LineMetrics\LM3\DataTypes;

abstract class BaseType implements BaseTypeInterface
{
    /**
     * Timestamp
     *
     * @var integer
     */
    protected $timestamp;

    /**
     * Value
     *
     * @var mixed
     */
    protected $value;

    public function __construct($value = null, $timestamp = null){
        $this->setValue($value);
        $this->setTimestamp($timestamp);
    }

    /**
     * @see \LineMetrics\LM3\DataTypes\BaseTypeInterface::setTimestamp()
     */
    public function setTimestamp($timestamp)
    {
        $this->timestamp = $timestamp;
        return $this;
    }

    /**
     * @see \LineMetrics\LM3\DataTypes\BaseTypeInterface::getTimestamp()
     */
    public function getTimestamp()
    {
        return $this->timestamp;
    }

    /**
     * @see \LineMetrics\LM3\DataTypes\BaseTypeInterface::setValue()
     */
    public function setValue($value)
    {
        $this->value = $value;
        return $this;
    }

    /**
     * @see \LineMetrics\LM3\DataTypes\BaseTypeInterface::getValue()
     */
    public function getValue()
    {
        return $this->value;
    }

    public abstract function __toString();
}