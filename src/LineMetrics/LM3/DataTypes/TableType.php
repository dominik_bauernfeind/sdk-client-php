<?php
namespace LineMetrics\LM3\DataTypes;

class TableType extends BaseType implements TableTypeInterface
{
    public function __construct($value = ""){
        parent::__construct($value);
    }

    /**
     * @see \LineMetrics\LM3\DataTypes\TableTypeInterface::getValue()
     */
    public function setValue( $value){
        parent::setValue( $value);
    }

    /**
     * @see \LineMetrics\LM3\DataTypes\TableTypeInterface::getValue()
     */
    public function getValue(){
        return (array)parent::getValue();
    }

  /**
   * Gets format for json representation
   *
     * @see JsonSerializable::jsonSerialize()
     */
    public function jsonSerialize(){

            return array("val"=> $this->getValue());
        
    }

    public  function __toString(){
        return sprintf( "Type: TableType, Value: %d", $this->getValue() );
    }

    /**
     *
     * @see \LineMetrics\LM3\DataTypes\BaseTypeInterface::getTypeName()
     */
    public function getTypeName(){
        return DataTypesEnum::DATA_TYPE_TABLE;
    }
}
