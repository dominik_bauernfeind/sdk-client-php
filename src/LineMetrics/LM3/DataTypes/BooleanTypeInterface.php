<?php
namespace LineMetrics\LM3\DataTypes;

interface BooleanTypeInterface extends BaseTypeInterface
{
    /**
     * Sets boolean value
     *
     * @param  $value boolean
     * @return BooleanTypeInterface
     */
    public function setValue($value);

    /**
     * Gets boolean value
     *
     * @return boolean
     */
    public function getValue();
}