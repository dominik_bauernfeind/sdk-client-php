<?php
namespace LineMetrics\LM3\DataTypes;

use LineMetrics\LM3\Exception\UnexpectedValueException;

class DataTypesFactory implements DataTypesFactoryInterface
{

    /**
     *
     * @see \LineMetrics\LM3\DataTypes\DataTypesFactoryInterface::getClassNameFor()
     * @throws UnexpectedValueException
     */
    public function getClassNameFor($typeName)
    {
        switch ($typeName) {
            case DataTypesEnum::DATA_TYPE_DOUBLE:
                return 'LineMetrics\\LM3\DataTypes\\DoubleType';
                break;

            case DataTypesEnum::DATA_TYPE_DOUBLE_AVERGAE:
                return 'LineMetrics\\LM3\DataTypes\\DoubleAverageType';
                break;

            case DataTypesEnum::DATA_TYPE_SIMPLE_DATE:
                return 'LineMetrics\\LM3\DataTypes\\SimpleDateType';
                break;

            case DataTypesEnum::DATA_TYPE_STRING:

                return 'LineMetrics\\LM3\DataTypes\\StringType';
                break;

            case DataTypesEnum::DATA_TYPE_TABLE:

                return 'LineMetrics\\LM3\DataTypes\\TableType';
                break;

            default:
                throw new UnexpectedValueException('Invalid data type "' . $typeName . '"');
                breaK;
        }
    }

    /**
     *
     * @see \LineMetrics\LM3\DataTypes\DataTypesFactoryInterface::getInterfaceNameFor()
     * @throws \OutOfBoundsException
     */
    public function getInterfaceNameFor($typeName)
    {
        switch ($typeName) {
            case DataTypesEnum::DATA_TYPE_DOUBLE:
                return 'LineMetrics\\LM3\DataTypes\\DoubleTypeInterface';
                break;

            case DataTypesEnum::DATA_TYPE_DOUBLE_AVERGAE:
                return 'LineMetrics\\LM3\DataTypes\\DoubleAverageTypeInterface';
                break;

            case DataTypesEnum::DATA_TYPE_SIMPLE_DATE:
                return 'LineMetrics\\LM3\DataTypes\\SimpleDateTypeInterface';
                break;

            case DataTypesEnum::DATA_TYPE_STRING:

                return 'LineMetrics\\LM3\DataTypes\\StringTypeInterface';
                break;

            case DataTypesEnum::DATA_TYPE_TABLE:

                return 'LineMetrics\\LM3\DataTypes\\TableTypeInterface';
                break;

            default:
                throw new UnexpectedValueException('Invalid data type "' . $typeName . '"');
                breaK;
        }
    }

    /**
     *
     * @see \LineMetrics\LM3\DataTypes\DataTypesFactoryInterface::createBoolean()
     * @return \LineMetrics\LM3\DataTypes\BooleanType
     */
    public function createBoolean($value, $timestamp = null)
    {
        return new BooleanType($value, $timestamp);
    }

    /**
     *
     * @see \LineMetrics\LM3\DataTypes\DataTypesFactoryInterface::createDouble()
     * @return \LineMetrics\LM3\DataTypes\DoubleType
     */
    public function createDouble($value, $timestamp = null)
    {
        return new DoubleType($value, $timestamp);
    }

    /**
     *
     * @see \LineMetrics\LM3\DataTypes\DataTypesFactoryInterface::createDoubleAverage()
     * @return \LineMetrics\LM3\DataTypes\DoubleAverageType
     */
    public function createDoubleAverage($value, $min, $max, $timestamp = null)
    {
        return new DoubleAverageType($value, $min, $max, $timestamp);
    }

    /**
     *
     * @see \LineMetrics\LM3\DataTypes\DataTypesFactoryInterface::createGeooCoord()
     * @return \LineMetrics\LM3\DataTypes\GeoCoordType
     */
    public function createGeooCoord($lat, $long, $timestamp = null)
    {
        return new GeoCoordType($lat, $long, $timestamp);
    }

    /**
     *
     * @see \LineMetrics\LM3\DataTypes\DataTypesFactoryInterface::createGeooAddress()
     * @return \LineMetrics\LM3\DataTypes\GeoAddressType
     */
    public function createGeooAddress($address, $lat, $long, $timestamp = null)
    {
        return new GeoAddressType($address, $lat, $long, $timestamp);
    }

    /**
     *
     * @see \LineMetrics\LM3\DataTypes\DataTypesFactoryInterface::createSimpleDate()
     * @return \LineMetrics\LM3\DataTypes\SimpleDateType
     */
    public function createSimpleDate($value, $timestamp = null)
    {
        return new SimpleDateType($value, $timestamp);
    }

    /**
     *
     * @see \LineMetrics\LM3\DataTypes\DataTypesFactoryInterface::createTable()
     * @return \LineMetrics\LM3\DataTypes\TableType
     */
    public function createTable($value)
    {
        return new TableType($value);
    }

    /**
     *
     * @see \LineMetrics\LM3\DataTypes\DataTypesFactoryInterface::createString()
     * @return \LineMetrics\LM3\DataTypes\StringType
     */
    public function createString($value, $timestamp = null)
    {
        return new StringType($value, $timestamp);
    }
}