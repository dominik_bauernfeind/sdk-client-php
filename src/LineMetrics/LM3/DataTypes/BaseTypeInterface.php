<?php
namespace LineMetrics\LM3\DataTypes;

interface BaseTypeInterface extends \JsonSerializable
{
    /**
     * Sets value
     *
     * @param mixed $value
     * @return BaseTypeInterface
     */
    public function setValue($value);

    /**
     * Gets value
     *
     * @return mixed
     */
    public function getValue();

    /**
     * Sets timestamp
     *
     * @param integer $timestamp
     * @return BaseTypeInterface
     */
    public function setTimestamp($timestamp);

    /**
     * Gets timestamp
     *
     * @return integer|null
     */
    public function getTimestamp();

    /**
     * Gets type name
     *
     * @return string;
     */
    public function getTypeName();
}

