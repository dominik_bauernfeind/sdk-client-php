<?php
namespace LineMetrics\LM3\DataTypes\Collection;

use LineMetrics\LM3\Collection\ContractInterface;
use LineMetrics\LM3\DataTypes\DataTypesSerializerInterface;

class TypedContract implements ContractInterface
{

    /**
     *  Data types serializer
     *
     * @var DataTypesSerializerInterface
     */
    protected $serializer;

    /**
     * Type class to check
     *
     * @var string
     */
    protected $typeClass;

    public function __construct(DataTypesSerializerInterface $serializer, $typeClass){
        $this->serializer = $serializer;
        $this->typeClass = $typeClass;
    }

    public function loadItem($value)
    {
        return $this->serializer->unserialize($value);
    }

    public function isValidItem($value)
    {
        return is_a($value, $this->typeClass);
    }
}