<?php
namespace LineMetrics\LM3\DataTypes\Collection;
use LineMetrics\LM3\DataTypes\Serializer\BooleanSerializer;
use LineMetrics\LM3\DataTypes\DataTypesCollection;

class BooleanCollection extends DataTypesCollection {
    public function __construct(array $items)
    {
        parent::__construct($items, new TypedContract( new BooleanSerializer(), 'LineMetrics\\LM3\\DataTypes\\BooleanType') );
    }
}
