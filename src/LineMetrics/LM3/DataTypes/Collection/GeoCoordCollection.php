<?php
namespace LineMetrics\LM3\DataTypes\Collection;
use LineMetrics\LM3\DataTypes\Serializer\GeoCoordSerializer;
use LineMetrics\LM3\DataTypes\DataTypesCollection;

class GeoCoordCollection extends DataTypesCollection {
    public function __construct(array $items)
    {
        parent::__construct($items, new TypedContract( new GeoCoordSerializer(), 'LineMetrics\\LM3\\DataTypes\\GeoCoordType') );
    }
}
