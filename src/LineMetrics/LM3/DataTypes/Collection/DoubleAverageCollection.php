<?php
namespace LineMetrics\LM3\DataTypes\Collection;
use LineMetrics\LM3\DataTypes\Serializer\DoubleAverageSerializer;
use LineMetrics\LM3\DataTypes\DataTypesCollection;

class DoubleAverageCollection extends DataTypesCollection {
    public function __construct(array $items)
    {
        parent::__construct($items, new TypedContract( new DoubleAverageSerializer(), 'LineMetrics\\LM3\\DataTypes\\DoubleAverageType') );
    }
}
