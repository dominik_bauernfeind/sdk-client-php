<?php
namespace LineMetrics\LM3\DataTypes\Collection;
use LineMetrics\LM3\DataTypes\Serializer\DoubleSerializer;
use LineMetrics\LM3\DataTypes\DataTypesCollection;

class DoubleCollection extends DataTypesCollection {
    public function __construct(array $items)
    {
        parent::__construct($items, new TypedContract( new DoubleSerializer(), 'LineMetrics\\LM3\\DataTypes\\DoubleType') );
    }
}
