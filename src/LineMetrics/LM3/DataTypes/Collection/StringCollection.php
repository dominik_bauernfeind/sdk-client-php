<?php
namespace LineMetrics\LM3\DataTypes\Collection;
use LineMetrics\LM3\DataTypes\Serializer\StringSerializer;
use LineMetrics\LM3\DataTypes\DataTypesCollection;

class StringCollection extends DataTypesCollection {
    public function __construct(array $items)
    {
        parent::__construct($items, new TypedContract( new StringSerializer(), 'LineMetrics\\LM3\\DataTypes\\StringType') );
    }
}
