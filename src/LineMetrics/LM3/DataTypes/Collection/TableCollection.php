<?php
namespace LineMetrics\LM3\DataTypes\Collection;
use LineMetrics\LM3\DataTypes\Serializer\TableSerializer;
use LineMetrics\LM3\DataTypes\DataTypesCollection;

class TableCollection extends DataTypesCollection {
    public function __construct(array $items)
    {
        parent::__construct($items, new TypedContract( new TableSerializer(), 'LineMetrics\\LM3\\DataTypes\\TableType') );
    }
}