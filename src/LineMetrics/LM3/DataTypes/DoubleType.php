<?php
namespace LineMetrics\LM3\DataTypes;

class DoubleType extends BaseType
{
    public function __construct($value = 0.0, $timestamp = null){
        parent::__construct($value, $timestamp );
    }

    /**
     * @see \LineMetrics\LM3\DataTypes\StringTypeInterface::getValue()
     * @return DoubleType
     */
    public function setValue( $value){
        parent::setValue( (double)$value);
    }

    /**
     * @see \LineMetrics\LM3\DataTypes\StringTypeInterface::getValue()
     */
    public function getValue(){
        return (double)parent::getValue();
    }

    /**
     * Gets format for json representation
     *
     * @see JsonSerializable::jsonSerialize()
     */
    public function jsonSerialize(){

        if( $this->timestamp ){
            return array(
                "val"=> $this->getValue(),
                "ts" =>(string)$this->timestamp
            );
        }
        else{
            return array("val"=> $this->getValue());
        }
    }

    public  function __toString(){
        return sprintf( "Type: DoubleType, Value: %f, Timestamp: %s", $this->getValue(), $this->getTimestamp() );
    }

    /**
     *
     * @see \LineMetrics\LM3\DataTypes\BaseTypeInterface::getTypeName()
     */
    public function getTypeName(){
        return DataTypesEnum::DATA_TYPE_DOUBLE;
    }
}
