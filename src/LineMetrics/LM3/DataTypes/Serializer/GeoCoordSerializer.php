<?php
namespace LineMetrics\LM3\DataTypes\Serializer;

use LineMetrics\LM3\DataTypes\DataTypesSerializerInterface;
use LineMetrics\LM3\DataTypes\GeoCoordType;

class GeoCoordSerializer implements DataTypesSerializerInterface
{

    /**
     * @see \LineMetrics\LM3\DataTypes\DataSerializerInterface::unserialize()
     * @return GeoCoordType
     */
    public function unserialize(\stdClass $payload)
    {
        $type = new GeoCoordType();
        $type->setLat($payload->lat);
        $type->setLong($payload->long);
        $type->setTimestamp($payload->ts);
        return $type;
    }
}