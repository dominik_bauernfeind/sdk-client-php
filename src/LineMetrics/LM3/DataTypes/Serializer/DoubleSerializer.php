<?php
namespace LineMetrics\LM3\DataTypes\Serializer;

use LineMetrics\LM3\DataTypes\DataTypesSerializerInterface;
use LineMetrics\LM3\DataTypes\DoubleType;

class DoubleSerializer implements DataTypesSerializerInterface
{

    /**
     * @see \LineMetrics\LM3\DataTypes\DataSerializerInterface::unserialize()
     * @return DoubleType
     */
    public function unserialize(\stdClass $payload)
    {
        $type = new DoubleType();
        $type->setTimestamp($payload->ts);
        $type->setValue($payload->val);
        return $type;
    }
}