<?php
namespace LineMetrics\LM3\DataTypes\Serializer;

use LineMetrics\LM3\DataTypes\DataTypesSerializerInterface;
use LineMetrics\LM3\DataTypes\TableType;

class TableSerializer implements DataTypesSerializerInterface
{

    /**
     * @see \LineMetrics\LM3\DataTypes\DataSerializerInterface::unserialize()
     * @return TableType
     */
    public function unserialize(\stdClass $payload)
    {
        $type = new TableType();
        $type->setValue($payload->val);
        return $type;
    }
}