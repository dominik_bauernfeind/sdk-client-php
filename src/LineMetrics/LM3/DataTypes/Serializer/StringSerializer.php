<?php
namespace LineMetrics\LM3\DataTypes\Serializer;

use LineMetrics\LM3\DataTypes\DataTypesSerializerInterface;
use LineMetrics\LM3\DataTypes\StringType;

class StringSerializer implements DataTypesSerializerInterface
{

    /**
     * @see \LineMetrics\LM3\DataTypes\DataSerializerInterface::unserialize()
     * @return StringType
     */
    public function unserialize(\stdClass $payload)
    {
        $type = new StringType();
        $type->setTimestamp($payload->ts);
        $type->setValue($payload->val);
        return $type;
    }
}