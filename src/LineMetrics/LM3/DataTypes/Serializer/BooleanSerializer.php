<?php
namespace LineMetrics\LM3\DataTypes\Serializer;

use LineMetrics\LM3\DataTypes\DataTypesSerializerInterface;
use LineMetrics\LM3\DataTypes\BooleanType;

class BooleanSerializer implements DataTypesSerializerInterface
{

    /**
     * @see \LineMetrics\LM3\DataTypes\DataSerializerInterface::unserialize()
     * @return BooleanType
     */
    public function unserialize(\stdClass $payload)
    {
        $type = new BooleanType();
        $type->setTimestamp($payload->ts);
        $type->setValue($payload->val);
        return $type;
    }
}