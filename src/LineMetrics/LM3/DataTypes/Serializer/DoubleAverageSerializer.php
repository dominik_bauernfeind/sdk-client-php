<?php
namespace LineMetrics\LM3\DataTypes\Serializer;

use LineMetrics\LM3\DataTypes\DataTypesSerializerInterface;
use LineMetrics\LM3\DataTypes\DoubleAverageType;

class DoubleAverageSerializer implements DataTypesSerializerInterface
{

    /**
     * @see \LineMetrics\LM3\DataTypes\DataSerializerInterface::unserialize()
     * @return DoubleAverageType
     */
    public function unserialize(\stdClass $payload)
    {
        $type = new DoubleAverageType();
        $type->setMin($payload->min);
        $type->setMax($payload->max);
        $type->setTimestamp($payload->ts);
        $type->setValue($payload->val);
        return $type;
    }
}