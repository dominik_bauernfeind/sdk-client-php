<?php
namespace LineMetrics\LM3\DataTypes;

interface DoubleAverageTypeInterface extends DoubleTypeInterface
{
    /**
     * Sets min value
     *
     * @param  double $min
     * @return DoubleAverageTypeInterface
     */
    public function setMin($min);

    /**
     * Gets min value
     *
     * @return double
     */
    public function getMin();


    /**
     * Sets max value
     *
     * @param  double $max
     * @return DoubleAverageTypeInterface
     */
    public function setMax($max);

    /**
     * Gets max value
     *
     * @return double
    */
    public function getMax();
}