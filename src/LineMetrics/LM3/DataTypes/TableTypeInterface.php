<?php
namespace LineMetrics\LM3\DataTypes;

interface TableTypeInterface extends BaseTypeInterface
{
    /**
     * Sets table value
     *
     * @param  array $value
     * @return TableTypeInterface
     */
    public function setValue($value);

    /**
     * Gets table value
     *
     * @return array
     */
    public function getValue();
}