<?php
namespace LineMetrics\LM3\DataTypes;

interface DataTypesFactoryAwareInterface
{
    /**
     * Sets data types factory
     *
     * @param DataTypesFactoryInterface|null $factory
     * @return DataTypesFactoryAwareInterface
     */
    public function setDataTypesFactory( DataTypesFactoryInterface $factory );


}