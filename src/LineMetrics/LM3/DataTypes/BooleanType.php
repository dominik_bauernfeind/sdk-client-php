<?php
namespace LineMetrics\LM3\DataTypes;

class BooleanType extends BaseType implements BooleanTypeInterface
{

    public function __construct($value, $timestamp = null){
        $this->setValue($value);
        $this->setTimestamp($timestamp);
    }

    /**
     * @see \LineMetrics\LM3\DataTypes\BooleanTypeInterface::getValue()
     * @return BooleanType
     */
    public function setValue( $value){
        return parent::setValue( (boolean)$value);
    }

    /**
     * @see \LineMetrics\LM3\DataTypes\BooleanTypeInterface::getValue()
     */
    public function getValue(){
        return (boolean)parent::getValue();
    }

  /**
   * Gets format for json representation
   *
     * @see JsonSerializable::jsonSerialize()
     */
    public function jsonSerialize(){
        if( $this->timestamp){
            return array(
                "val"=> $this->getValue() ? 1 : 0,
                "ts" =>(string)$this->timestamp
            );
        }
        else{
            return array("val"=> $this->getValue() ? 1 : 0);
        }
    }

    public  function __toString(){
        return sprintf( "Type: BooleanType, Value: %d, Timestamp: %s", $this->getValue(), $this->getTimestamp() );
    }

    /**
     *
     * @see \LineMetrics\LM3\DataTypes\BaseTypeInterface::getTypeName()
     */
    public function getTypeName(){
        return "Boolean";
    }
}
