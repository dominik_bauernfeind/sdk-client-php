<?php
namespace LineMetrics\LM3\DataTypes;

class StringType extends BaseType implements StringTypeInterface
{
    public function __construct($value = "", $timestamp = null){
        parent::__construct($value, $timestamp );
    }

    /**
     * @see \LineMetrics\LM3\DataTypes\StringTypeInterface::getValue()
     */
    public function setValue( $value){
        parent::setValue( (string)$value);
    }

    /**
     * @see \LineMetrics\LM3\DataTypes\StringTypeInterface::getValue()
     */
    public function getValue(){
        return (string)parent::getValue();
    }

  /**
   * Gets format for json representation
   *
     * @see JsonSerializable::jsonSerialize()
     */
    public function jsonSerialize(){

        if( $this->timestamp ){
            return array(
                "val"=> $this->getValue(),
                "ts" =>(string)$this->timestamp
            );
        }
        else{
            return array("val"=> $this->getValue());
        }
    }

    public  function __toString(){
        return sprintf( "Type: StringType, Value: %d, Timestamp: %s", $this->getValue(), $this->getTimestamp() );
    }

    /**
     *
     * @see \LineMetrics\LM3\DataTypes\BaseTypeInterface::getTypeName()
     */
    public function getTypeName(){
        return DataTypesEnum::DATA_TYPE_STRING;
    }
}
