<?php
namespace LineMetrics\LM3\DataTypes;

interface GeoCoordTypeInterface extends BaseTypeInterface
{
    /**
     * Sets lat value
     *
     * @param  double $lat
     * @return GeoCoordTypeInterface
     */
    public function setLat($lat);

    /**
     * Gets lat value
     *
     * @return double
     */
    public function getLat();


    /**
     * Sets long value
     *
     * @param  double $long
     * @return GeoCoordTypeInterface
     */
    public function setLong($long);

    /**
     * Gets long value
     *
     * @return double
    */
    public function getLong();

    /**
     * Sets lat and long value
     *
     * @param  array $value
     * @return GeoCoordTypeInterface
     */
    public function setValue($value);

    /**
     * Gets lat and long value
     *
     * @return array
     */
    public function getValue();
}