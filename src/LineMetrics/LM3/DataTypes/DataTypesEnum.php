<?php
namespace LineMetrics\LM3\DataTypes;

abstract class DataTypesEnum
{
    const DATA_TYPE_DOUBLE = "Double";

    const DATA_TYPE_DOUBLE_AVERGAE = "DoubleAverage";

    const DATA_TYPE_STRING = "String";

    const DATA_TYPE_SIMPLE_DATE = "Timestamp";

    const DATA_TYPE_GEO_COORD = "GeoCoord";

    const DATA_TYPE_GEO_ADDRESS = "GeoAddress";

    const DATA_TYPE_TABLE = "Table";
}

