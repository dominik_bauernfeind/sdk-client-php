<?php
namespace LineMetrics\LM3\DataTypes;

interface GeoAddressTypeInterface extends GeoCoordTypeInterface
{
    /**
     * Sets address value
     *
     * @param  string $value
     * @return GeoAddressTypeInterface
     */
    public function setValue($value);

    /**
     * Gets address value
     *
     * @return string
     */
    public function getValue();
}