<?php
namespace LineMetrics\LM3\DataTypes;

interface StringTypeInterface extends BaseTypeInterface
{
    /**
     * Sets string value
     *
     * @param  string $value
     * @return StringTypeInterface
     */
    public function setValue($value);

    /**
     * Gets string value
     *
     * @return string
     */
    public function getValue();
}