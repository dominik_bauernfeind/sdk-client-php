<?php
namespace LineMetrics\LM3\DataTypes;

class GeoCoordType extends BaseType implements GeoCoordTypeInterface
{

    /**
     * Min value
     *
     * @var mixed
     */
    protected $lat;

    /**
     * Max value
     *
     * @var mixed
     */
    protected $long;



    public function __construct($lat = 0.0, $long = 0.0, $timestamp = null)
    {
        parent::__construct(null, $timestamp);
        $this->setLat($lat);
        $this->setLong($long);
    }

    /**
     * @see \LineMetrics\LM3\DataTypes\GeoCoordTypeInterface::setValue()
     * @return GeoCoordType
     */
    public function setValue($value){
        return $this;
    }

    /**
     * @see \LineMetrics\LM3\DataTypes\GeoCoordTypeInterface::getValue()
     */
    public function getValue(){
        return null;
    }

    /**
     *
     * @see \LineMetrics\LM3\DataTypes\GeoCoordTypeInterface::setLat()
     * @return GeoCoordType
     */
    public function setLat($lat)
    {
        $this->lat = (double) $lat;
        return $this;
    }

    /**
     *
     * @see \LineMetrics\LM3\DataTypes\GeoCoordTypeInterface::getLat()
     */
    public function getLat()
    {
        return (double) $this->lat;
    }

    /**
     * @see \LineMetrics\LM3\DataTypes\GeoCoordTypeInterface::setLong()
     * @return GeoCoordType
     */
    public function setLong($long)
    {
        $this->long = (double) $long;
        return $this;
    }

    /**
     * @see \LineMetrics\LM3\DataTypes\GeoCoordTypeInterface::getLong()
     */
    public function getLong()
    {
        return (double) $this->long;
    }


    /**
     * Gets json representation
     *
     * @see JsonSerializable::jsonSerialize()
     */
    public function jsonSerialize()
    {
        if( $this->timestamp){
            return array(
                "lat" => $this->getLat(),
                "long" => $this->getLong(),
                "ts" => (string)$this->timestamp,
            );
        }
        else{
            return array(
                "lat" => $this->getLat(),
                "long" => $this->getLong(),
            );
        }
    }

    public  function __toString(){
        return sprintf( "Type: GeoCoordType, Lat: %f, Long: %f, Timestamp: %s", $this->getValue(), $this->getLat(), $this->getLong(), $this->getTimestamp() );
    }

    /**
     *
     * @see \LineMetrics\LM3\DataTypes\BaseTypeInterface::getTypeName()
     */
    public function getTypeName(){
        return DataTypesEnum::DATA_TYPE_GEO_COORD;
    }
}
