<?php
namespace LineMetrics\LM3\DataTypes;

use LineMetrics\LM3\Collection\BaseCollection;
use LineMetrics\LM3\Collection\ContractInterface;

class DataTypesCollection extends BaseCollection implements DataTypesCollectionInterface
{
    public function __construct(array $items, ContractInterface $contract = null){
        parent::__construct($items, $contract ? $contract : new DataTypesContract());
    }
}
