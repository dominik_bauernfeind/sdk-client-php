<?php
namespace LineMetrics\LM3\DataTypes;

class GeoAddressType extends GeoCoordType implements GeoAddressTypeInterface
{

    public function __construct( $address = '', $lat = 0.0, $long = 0.0, $timestamp = null)
    {
        parent::__construct($lat, $long, $timestamp);
        $this->setValue($address);
    }

    /**
     * @see \LineMetrics\LM3\DataTypes\StringTypeInterface::getValue()
     */
    public function setValue( $value){
        $this->value = (string)$value;
        return $this;
    }

    /**
     * @see \LineMetrics\LM3\DataTypes\StringTypeInterface::getValue()
     */
    public function getValue(){
        return (string)$this->value;
    }

    public  function __toString(){
        return sprintf( "Type: GeoAddressType, Value: %s, Lat: %f, Long: %f, Timestamp: %d", $this->getValue(), $this->getLat(), $this->getLong(), $this->getTimestamp() );
    }

    /**
     * Gets json representation
     *
     * @see \LineMetrics\LM3\DataTypes\GeoCoordType::jsonSerialize()
     */
    public function jsonSerialize()
    {
        return array_merge( parent::jsonSerialize(), array(
            "val" => $this->value
        ));
    }

    /**
     *
     * @see \LineMetrics\LM3\DataTypes\BaseTypeInterface::getTypeName()
     */
    public function getTypeName(){
        return DataTypesEnum::DATA_TYPE_GEO_ADDRESS;
    }
}
