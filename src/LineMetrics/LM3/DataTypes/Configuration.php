<?php
namespace LineMetrics\LM3\DataTypes;

class Configuration implements ConfigurationInterface
{

    protected $input;

    protected $output;

    public function __construct($input = null, $output = null)
    {
        $this->setInput($input);
        $this->setOutput($output);
    }

    /**
     * Sets intput type
     *
     * @param string $input
     * @return \LineMetrics\LM3\DataTypes\Configuration
     */
    public function setInput($input)
    {
        $this->input = $input;
        return $this;
    }

    /**
     *
     * @see \LineMetrics\LM3\ObjectTypes\ConfigurationInterface::getInput()
     */
    public function getInput()
    {
        return $this->input;
    }

    /**
     * Sets output type
     *
     * @param string $output
     * @return \LineMetrics\LM3\DataTypes\Configuration
     */
    public function setOutput($output)
    {
        $this->output = $output;
        return $this;
    }

    /**
     * @see \LineMetrics\LM3\ObjectTypes\ConfigurationInterface::getOutput()
     */
    public function getOutput()
    {
        return $this->output;
    }
}