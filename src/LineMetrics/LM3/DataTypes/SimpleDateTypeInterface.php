<?php
namespace LineMetrics\LM3\DataTypes;

interface SimpleDateTypeInterface extends BaseTypeInterface
{
    /**
     * Sets datetime value as timestamp
     *
     * @param  integer $value
     * @return SimpleDateInterface
     */
    public function setValue($value);

    /**
     * Gets datetime value as timestamp
     *
     * @return string
     */
    public function getValue();
}