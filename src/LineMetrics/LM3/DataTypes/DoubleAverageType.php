<?php
namespace LineMetrics\LM3\DataTypes;

class DoubleAverageType extends DoubleType implements DoubleAverageTypeInterface
{
    /**
     * Min value
     *
     * @var mixed
     */
    protected $min;

    /**
     * Max value
     *
     * @var mixed
     */
    protected $max;


    public function __construct($value = 0.0, $min = 0.0, $max= 0.0, $timestamp = null){
        parent::__construct($value, $timestamp );
        $this->setMin($min);
        $this->setMax($max);
    }

    /**
     * @see \LineMetrics\LM3\DataTypes\DoubleAverageTypeInterface::setMin()
     * @return DoubleAverageType
     */
    public function setMin( $min){
       $this->min = (double)$min;
       return $this;
    }

    /**
     * @see \LineMetrics\LM3\DataTypes\DoubleAverageTypeInterface::getMin()
     */
    public function getMin(){
        return (double)$this->min;
    }

    /**
     * @see \LineMetrics\LM3\DataTypes\DoubleAverageTypeInterface::setMax()
     * @return DoubleAverageType
     */
    public function setMax( $max){
       $this->max = (double)$max;
       return $this;
    }

    /**
     * @see \LineMetrics\LM3\DataTypes\DoubleAverageTypeInterface::getMax()
     */
    public function getMax(){
        return (double)$this->max;
    }

  /**
   * Gets format for json representation
   *
     * @see JsonSerializable::jsonSerialize()
     */
    public function jsonSerialize(){

        if( $this->timestamp){
            return array(
                "ts" =>(string)$this->timestamp,
                "val"=> $this->getValue(),
                "min"=> $this->getMin(),
                "max"=> $this->getMax()
            );
        }
        else{
            return array(
                "val"=> $this->getValue(),
                "min"=> $this->getMin(),
                "max"=> $this->getMax()
            );
        }
    }

    public  function __toString(){
        return sprintf( "Type: DoubleType, Value: %f, Min: %f, Max: %f, Timestamp: %s", $this->getValue(), $this->getMin(), $this->getMax(), $this->getTimestamp() );
    }

    /**
     *
     * @see \LineMetrics\LM3\DataTypes\BaseTypeInterface::getTypeName()
     */
    public function getTypeName(){
        return DataTypesEnum::DATA_TYPE_DOUBLE_AVERGAE;
    }
}
