<?php
namespace LineMetrics\LM3\DataTypes;

class SimpleDateType extends BaseType implements SimpleDateTypeInterface
{

    public function __construct( $value = null, $timestamp = null){
        parent::__construct( is_null($value) ? strval( time() * 1000) : $value, $timestamp );
    }

    /**
     * @see \LineMetrics\LM3\DataTypes\SimpleDateInterface::setValue()
     */
    public function setValue($value)
    {
        $this->value = $value;
        return $this;
    }

    /**
     * @see \LineMetrics\LM3\DataTypes\SimpleDateInterface::getValue()
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
   * Gets format for json representation
   *
     * @see JsonSerializable::jsonSerialize()
     */
    public function jsonSerialize(){
        if( $this->timestamp ){
            return array(
                "val"=> $this->getValue(),
                "ts" =>(string)$this->timestamp
            );
        }
        else{
            return array("val"=> $this->getValue());
        }
    }

    public  function __toString(){
        return sprintf( "Type: SimpleDateType, Value: %d, Value Formatted: %s, Timestamp: %s", $this->getValue(), date( \DateTime::RFC3339 , $this->getValue()), $this->getTimestamp() );
    }


    /**
     *
     * @see \LineMetrics\LM3\DataTypes\BaseTypeInterface::getTypeName()
     */
    public function getTypeName(){
        return DataTypesEnum::DATA_TYPE_SIMPLE_DATE;
    }
}
