<?php
namespace LineMetrics\LM3\RequestTypes;

class AssetRequest extends ObjectRequest implements AssetRequestInterface
{

    protected $limit;

    protected $objectType;

    protected $offset;

    protected $loadPropertyData;

    protected $loadInputRef;

    /**
     *
     * @see \LineMetrics\LM3\RequestTypes\AssetRequestInterface::setLimit()
     */
    public function setLimit($limit)
    {
        $this->limit = $limit;
        return $this;
    }

    /**
     *
     * @see \LineMetrics\LM3\RequestTypes\AssetRequestInterface::getLimit()
     */
    public function getLimit()
    {
        return $this->limit;
    }

    /**
     *
     * @see \LineMetrics\LM3\RequestTypes\AssetRequestInterface::setObjectType()
     */
    public function setObjectType($objectType)
    {
        $this->objectType = $objectType;
        return $this;
    }

    /**
     *
     * @see \LineMetrics\LM3\RequestTypes\AssetRequestInterface::getObjectType()
     */
    public function getObjectType()
    {
        return $this->objectType;
    }

    /**
     *
     * @see \LineMetrics\LM3\RequestTypes\AssetRequestInterface::setOffset()
     */
    public function setOffset($offset)
    {
        $this->offset = $offset;
        return $this;
    }

    /**
     *
     * @see \LineMetrics\LM3\RequestTypes\AssetRequestInterface::getOffset()
     */
    public function getOffset()
    {
        return $this->offset;
    }

    /**
     *
     * @see \LineMetrics\LM3\RequestTypes\AssetRequestInterface::setLoadInputRef()
     */
    public function setLoadInputRef($load)
    {
        $this->loadInputRef = $load;
        return $this;
    }

    /**
     *
     * @see \LineMetrics\LM3\RequestTypes\AssetRequestInterface::getLoadInputRef()
     */
    public function getLoadInputRef()
    {
        return $this->loadInputRef;
    }

    /**
     *
     * @see \LineMetrics\LM3\RequestTypes\AssetRequestInterface::setLoadPropertyData()
     */
    public function setLoadPropertyData($load)
    {
        $this->loadPropertyData = $load;
        return $this;
    }

    /**
     *
     * @see \LineMetrics\LM3\RequestTypes\AssetRequestInterface::getLoadPropertyData()
     */
    public function getLoadPropertyData()
    {
        return $this->loadPropertyData;
    }

    public function getQueryParams()
    {
        $params = array();

        if ( is_int($this->limit) && $this->limit > 0) {
            $params['limit'] = $this->limit;
        }

        if ( is_int($this->offset) && $this->offset > -1 ) {
            $params['offset'] = $this->offset;
        }

        if ( strlen($this->objectType) ) {
            $params['object_type'] = $this->objectType;
        }

        if ( $this->loadPropertyData ) {
            $params['load_property_data'] = 1;
        }

        if ( $this->loadInputRef ) {
            $params['loadInputRef'] = 1;
        }

        return $params;
    }
}
