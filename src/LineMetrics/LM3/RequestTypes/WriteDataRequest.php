<?php
namespace LineMetrics\LM3\RequestTypes;

use LineMetrics\LM3\DataTypes\DataTypesCollectionInterface;

class WriteDataRequest extends ObjectRequest implements WriteDataRequestInterface
{

    /**
     * Data to write
     *
     * @var DataTypesCollectionInterface
     */
    protected $data;


    /**
     * Encoding flags
     *
     * @var integer
     */
    protected $encodingFlags = 0;

    /**
     * @see \LineMetrics\LM3\RequestTypes\WriteDataRequestInterface::setData()
     */
    public function setData(\Traversable $data = null){
        $this->data = $data;
        return $this;
    }

    /**
     * @see \LineMetrics\LM3\RequestTypes\WriteDataRequestInterface::getData()
     */
    public function getData(){
        return $this->data;
    }

    /**
     * @see \LineMetrics\LM3\RequestTypes\WriteDataRequestInterface::setEncodingFlags()
     */
    public function setEncodingFlags($flags){
        $this->encodingFlags = $flags;
        return $this;
    }

    /**
     * @see \LineMetrics\LM3\RequestTypes\WriteDataRequestInterface::getEncodingFlags()
     */
    public function getEncodingFlags(){
        return $this->encodingFlags;
    }
}
