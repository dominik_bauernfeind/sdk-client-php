<?php
namespace LineMetrics\LM3\RequestTypes;

class TemplatesRequest extends BaseRequest implements TemplatesRequestInterface
{
    /**
     * @see \LineMetrics\LM3\RequestTypes\BaseRequestInterface::toRequestParams()
     */
    public function getQueryParams(){
        return array();
    }
}