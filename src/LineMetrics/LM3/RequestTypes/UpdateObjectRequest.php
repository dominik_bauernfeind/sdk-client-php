<?php
namespace LineMetrics\LM3\RequestTypes;

class UpdateObjectRequest extends ObjectRequest implements UpdateObjectRequestInterface
{

    /**
     * Name
     *
     * @var string
     */
    protected $name;

    /**
     * Parent id
     *
     * @var string
     */
    protected $parent;

    /**
     * Alias
     *
     * @var string
     */
    protected $alias;

    /**
     * Custom key
     *
     * @var string
     */
    protected $customKey;

    public function __construct($objectId , $options = array())
    {
        parent::__construct($objectId);

        if (isset($options['name'])) {
            $this->setName($options['name']);
        }

        if (isset($options['parent'])) {
            $this->setParent($options['parent']);
        }

        if(isset($options['alias'])){
            $this->setAlias($options['alias']);
        }

        if(isset($options['customKey'])){
            $this->setCustomKey($options['customKey']);
        }
    }

    /**
     *
     * @see \LineMetrics\LM3\RequestTypes\UpdateObjectRequestInterface::setName()
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     *
     * @see \LineMetrics\LM3\RequestTypes\UpdateObjectRequestInterface::getName()
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     *
     * @see \LineMetrics\LM3\RequestTypes\UpdateObjectRequestInterface::setParent()
     */
    public function setParent($parent)
    {
        $this->parent = $parent;
        return $this;
    }

    /**
     *
     * @see \LineMetrics\LM3\RequestTypes\UpdateObjectRequestInterface::getParent()
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     *
     * @see \LineMetrics\LM3\RequestTypes\UpdateObjectRequestInterface::setAlias()
     */
    public function setAlias($alias)
    {
        $this->alias = $alias;
        return $this;
    }

    /**
     *
     * @see \LineMetrics\LM3\RequestTypes\UpdateObjectRequestInterface::getAlias()
     */
    public function getAlias()
    {
        return $this->alias;
    }

    /**
     *
     * @see \LineMetrics\LM3\RequestTypes\UpdateObjectRequestInterface::setCustomKey()
     */
    public function setCustomKey($customKey)
    {
        $this->customKey = $customKey;
        return $this;
    }

    /**
     *
     * @see \LineMetrics\LM3\RequestTypes\UpdateObjectRequestInterface::getCustomKey()
     */
    public function getCustomKey()
    {
        return $this->customKey;
    }

    /**
     *
     * @see \LineMetrics\LM3\RequestTypes\BaseRequestInterface::toRequestParams()
     */
    public function getQueryParams()
    {
        $params = array();

        if (strlen($this->alias)) {
            $params['alias'] = $this->alias;
        }

        if (strlen($this->customKey)) {
            $params['custom_key'] = $this->customKey;
        }

        if (strlen($this->name)) {
            $params['name'] = $this->name;
        }

        if (strlen($this->parent)) {
            $params['parent'] = $this->parent;
        }

        return $params;
    }
}
