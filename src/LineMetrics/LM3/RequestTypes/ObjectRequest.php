<?php
namespace LineMetrics\LM3\RequestTypes;

use LineMetrics\LM3\RequestTypes\BaseRequest;

class ObjectRequest extends BaseRequest implements ObjectRequestInterface
{

    public function __construct( $objectId=null ){
        $this->setObjectId($objectId);
    }

    /**
     *
     * @see \LineMetrics\LM3\RequestTypes\BaseRequestInterface::setObjectId()
     */
    public function setObjectId($objectId)
    {
        $this->objectId = $objectId;
        return $this;
    }

    /**
     *
     * @see \LineMetrics\LM3\RequestTypes\BaseRequestInterface::getObjectId()
     */
    public function getObjectId()
    {
        return $this->objectId;
    }

	/**
     * @see \LineMetrics\LM3\RequestTypes\BaseRequest::jsonSerialize()
     */
    public function jsonSerialize()
    {
        $json = parent::jsonSerialize();
        $json['objectId'] = $this->getObjectId();
        return $json;
    }
}