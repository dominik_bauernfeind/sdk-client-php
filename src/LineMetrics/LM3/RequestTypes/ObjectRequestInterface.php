<?php
namespace LineMetrics\LM3\RequestTypes;

use LineMetrics\LM3\RequestTypes\BaseRequestInterface;

interface ObjectRequestInterface extends BaseRequestInterface
{
    /**
     * Sets object id
     *
     * @param string|null $objectId
     * @return BaseRequestInterface
     */
    public function setObjectId($objectId);
    
    /**
     * Gets object id
     *
     * @return string|null
    */
    public function getObjectId();

}