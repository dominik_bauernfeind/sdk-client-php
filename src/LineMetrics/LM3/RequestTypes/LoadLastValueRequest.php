<?php
namespace LineMetrics\LM3\RequestTypes;
use LineMetrics\LM3\ObjectTypes\DataFunctionsEnum;

class LoadLastValueRequest extends ObjectRequest implements LoadLastValueRequestInterface
{
    /**
     * @see \LineMetrics\LM3\RequestTypes\BaseRequestInterface::toRequestParams()
     */
    public function getQueryParams()
    {
        $params = array();
        $params['function'] = DataFunctionsEnum::FUNCTION_LAST_VALUE;
        return $params;
    }
}