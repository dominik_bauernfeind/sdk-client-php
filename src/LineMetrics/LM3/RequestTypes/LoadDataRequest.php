<?php
namespace LineMetrics\LM3\RequestTypes;

class LoadDataRequest extends ObjectRequest implements LoadDataRequestInterface
{

    protected $from;

    protected $to;

    protected $tz;

    protected $granularity;

    protected $func;

    /**
     * @see \LineMetrics\LM3\RequestTypes\LoadDataRequestInterface::setFrom()
     * @return LoadDataRequest
     */
    public function setFrom($from)
    {
        $this->from = $from;
        return $this;
    }

    /**
     * @see \LineMetrics\LM3\RequestTypes\LoadDataRequestInterface::getFrom()
     */
    public function getFrom()
    {
        return $this->from;
    }

    /**
     * @see \LineMetrics\LM3\RequestTypes\LoadDataRequestInterface::setTo()
     * @return LoadDataRequest
     */
    public function setTo($to)
    {
        $this->to = $to;
        return $this;
    }

    /**
     * @see \LineMetrics\LM3\RequestTypes\LoadDataRequestInterface::getTo()
     */
    public function getTo()
    {
        return $this->to;
    }

    /**
     * @see \LineMetrics\LM3\RequestTypes\LoadDataRequestInterface::setTimezone()
     * @return LoadDataRequest
     */
    public function setTimezone($tz)
    {
        $this->tz = $tz;
        return $this;
    }

    /**
     * @see \LineMetrics\LM3\RequestTypes\LoadDataRequestInterface::getTimezone()
     */
    public function getTimezone()
    {
        return $this->tz;
    }

    /**
     * @see \LineMetrics\LM3\RequestTypes\LoadDataRequestInterface::setGranularity()
     * @return LoadDataRequest
     */
    public function setGranularity($granularity)
    {
        $this->granularity = $granularity;
        return $this;
    }

    /**
     * @see \LineMetrics\LM3\RequestTypes\LoadDataRequestInterface::getGranularity()
     */
    public function getGranularity()
    {
        return $this->granularity;
    }

    /**
     * @see \LineMetrics\LM3\RequestTypes\LoadDataRequestInterface::setFunction()
     * @return LoadDataRequest
     */
    public function setFunction($function)
    {
        $this->func = $function;
        return $this;
    }

    /**
     * @see \LineMetrics\LM3\RequestTypes\LoadDataRequestInterface::getFunction()
     */
    public function getFunction()
    {
        return $this->func;
    }

    /**
     * @see \LineMetrics\LM3\RequestTypes\BaseRequestInterface::toRequestParams()
     */
    public function getQueryParams()
    {
        $params = array();
        $params['time_from'] = (string) $this->from;
        $params['time_to'] = (string) $this->to;

        if (strlen($this->func)) {
            $params['function'] = $this->func;
        }

        if (strlen($this->tz)) {
            $params['time_zone'] = $this->tz;
        }

        if (strlen($this->granularity)) {
            $params['granularity'] = $this->granularity;
        }

        return $params;
    }
}