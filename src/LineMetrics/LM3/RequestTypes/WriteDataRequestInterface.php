<?php
namespace LineMetrics\LM3\RequestTypes;

interface WriteDataRequestInterface extends ObjectRequestInterface
{
    /**
     * Sets data
     *
     * @param \Traversable|null $data
     * @return WriteDataRequestInterface
     */
    public function setData(\Traversable $data = null);

    /**
     * Gets data
     *
     * @return \Traversable|null
     */
    public function getData();

    /**
     * Sets encoding flags
     *
     * @param integer $flags
     */
    public function setEncodingFlags($flags);

    /**
     * Gets encoding flags
     *
     * @return integer
     */
    public function getEncodingFlags();
}