<?php
namespace LineMetrics\LM3\RequestTypes;

interface BaseRequestInterface extends \JsonSerializable
{
     /**
     * Builds an array of request params
     *
     *  @return array
     */
    public function getQueryParams();

}