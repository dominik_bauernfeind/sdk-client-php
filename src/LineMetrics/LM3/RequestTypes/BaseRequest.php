<?php
namespace LineMetrics\LM3\RequestTypes;

abstract class BaseRequest implements BaseRequestInterface
{
    public function getQueryParams(){
        return array();
    }

    public function jsonSerialize(){
        return array(
            'queryParams' => $this->getQueryParams()
        );
    }
}
