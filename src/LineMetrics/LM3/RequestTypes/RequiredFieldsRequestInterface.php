<?php
namespace LineMetrics\LM3\RequestTypes;

interface RequiredFieldsRequestInterface extends BaseRequestInterface
{
    public function setTemplateUid( $uid );

    public function getTemplateUid();

}