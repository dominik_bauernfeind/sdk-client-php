<?php
namespace LineMetrics\LM3\RequestTypes;

interface LoadDataRequestInterface extends ObjectRequestInterface
{
    /**
     * Sets time from in ms
     *
     * @param int $from
     * @return LoadDataRequestInterface
     */
    public function setFrom( $from);

    /**
     * Gets time from in ms
     *
     * @return int|null
     */
    public function getFrom();

    /**
     * Sets time to in ms
     *
     * @param int $to
     * @return LoadDataRequestInterface
     */
    public function setTo( $to );

    /**
     * Gets time to in ms
     *
     * @return int
     */
    public function getTo();

    /**
     * Sets data function
     *
     * @param string $function
     * @return LoadDataRequestInterface
     */
    public function setFunction( $function );

    /**
     * Gets data function
     *
     * @return string
     */
    public function getFunction();

    /**
     * Set timezone identifier
     *
     * @param string $tz
     * @return LoadDataRequestInterface
     */
    public function setTimezone( $tz );

    /**
     * Gets timezone identifier
     *
     * @return string
     */
    public function getTimezone();

    /**
     * Sets data granularity
     *
     * @param string $granularity
     * @return LoadDataRequestInterface
     */
    public function setGranularity($granularity);

    /**
     * Gets data granularity
     *
     * @return string
     */
    public function getGranularity();
}
