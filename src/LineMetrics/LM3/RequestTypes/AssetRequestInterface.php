<?php
namespace LineMetrics\LM3\RequestTypes;

interface AssetRequestInterface extends ObjectRequestInterface
{
    /**
     * Sets limit
     *
     * @param int|null $limit
     * @return AssetRequestInterface
     */
    public function setLimit($limit);

    /**
     * Gets limit
     *
     * @return int|null
     */
    public function getLimit();

    /**
     * Sets offset
     *
     * @param int|null $offset
     * @return AssetRequestInterface
     */
    public function setOffset($offset);

    /**
     * Gets offset
     *
     * @return int|null
     */
    public function getOffset();

    /**
     * Sets object type
     *
     * @param string|null $objectType
     * @return AssetRequestInterface
     */
    public function setObjectType($objectType);

    /**
     * Gets object type
     *
     * @return string|null
     */
    public function getObjectType();

    /**
     * Sets load property data flag
     *
     * @param boolean|null $load
     * @return AssetRequestInterface
     */
    public function setLoadPropertyData( $load );

    /**
     * Gets load property data flag
     *
     * @return boolean|null
     */
    public function getLoadPropertyData();

    /**
     * Sets load input ref flag
     *
     * @param boolean|null $load
     * @return AssetRequestInterface
     */
    public function setLoadInputRef( $load );

    /**
     * Gets load input ref flag
     *
     * @return boolean|null
     */
    public function getLoadInputRef();

}

