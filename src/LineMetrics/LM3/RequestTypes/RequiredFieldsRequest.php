<?php
namespace LineMetrics\LM3\RequestTypes;

class RequiredFieldsRequest extends BaseRequest implements RequiredFieldsRequestInterface
{

    protected $templateUid;

    /**
     *
     * @see \LineMetrics\LM3\RequestTypes\RequiredFieldsRequestInterface::setTemplateUid()
     */
    public function setTemplateUid($uid)
    {
        $this->templateUid = $uid;
        return $this;
    }

    /**
     *
     * @see \LineMetrics\LM3\RequestTypes\RequiredFieldsRequestInterface::getTemplateUid()
     */
    public function getTemplateUid()
    {
        return $this->templateUid;
    }

    /**
     *
     * @see \LineMetrics\LM3\RequestTypes\BaseRequestInterface::toRequestParams()
     */
    public function getQueryParams()
    {
        return array();
    }
}
