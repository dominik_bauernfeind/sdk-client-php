<?php
namespace LineMetrics\LM3\RequestTypes;

interface UpdateObjectRequestInterface extends ObjectRequestInterface
{
    /**
     * Sets new name
     *
     * @param string|null $name
     * @return UpdateRessourceRequest
     */
    public function setName($name);


    /**
     * Gets new name
     *
     * @return string|null
     */
    public function getName();

    /**
     * Sets new parent id
     *
     * @param string|null $parent
     * @return UpdateRessourceRequest
     */
    public function setParent($parent);


    /**
     * Gets new parent id
     *
     * @return string
     */
    public function getParent();

    /**
     * Sets custom key
     *
     * @param string|null $setCustomKey
     * @return UpdateObjectRequestInterface
     */
    public function setCustomKey($setCustomKey);

    /**
     * Gets custom key
     *
     * @return string|null
     */
    public function getCustomKey();

    /**
     * Sets alias
     *
     * @param string|null $alias
     * @return UpdateObjectRequestInterface
     */
    public function setAlias($alias);

    /**
     * Gets alias
     *
     * @return string|null
     */
    public function getAlias();
}
