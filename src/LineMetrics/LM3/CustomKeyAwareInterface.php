<?php
namespace LineMetrics\LM3;

interface CustomKeyAwareInterface
{
    /**
     * Sets custom key
     *
     * @param string|null
     * @return CustomKeyAwareInterface
     */
    public function setCustomKey($customKey);

    /**
     * Gets custom key
     *
     * @return string|null
     */
    public function getCustomKey();

}