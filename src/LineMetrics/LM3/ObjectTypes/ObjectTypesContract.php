<?php
namespace LineMetrics\LM3\ObjectTypes;

use LineMetrics\LM3\Collection\ContractInterface;
use LineMetrics\LM3\DataTypes\BaseTypeInterface;

class ObjectTypesContract implements ContractInterface
{
    /**
     * The serializer
     *
     * @var ObjectTypesSerializerInterface
     */
    protected $serializer;

    public function __construct( ObjectTypesSerializerInterface $serializer ){
        $this->serializer = $serializer;
    }
    public function loadItem($value)
    {
        return $this->serializer->unserialize($value);
    }

    public function isValidItem($value)
    {
        return $value instanceof BaseTypeInterface;
    }
}