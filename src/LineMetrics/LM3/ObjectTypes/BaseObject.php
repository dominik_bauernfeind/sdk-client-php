<?php
namespace LineMetrics\LM3\ObjectTypes;
use \LineMetrics\LM3\Api\ApiAwareTrait;

class BaseObject implements BaseObjectInterface
{

    use ApiAwareTrait;

    /**
     * Object id
     *
     * @var string
     */
    protected $objectId = null;

    /**
     * Object type
     *
     * @var string
     */
    protected $objectType = null;

    /**
     * Template id
     *
     * @var string
     */
    protected $templateId = null;

    /**
     * Parent id
     *
     * @var string
     */
    protected $parentId = null;

    /**
     * Payload
     *
     * @var array
     */
    protected $payload = array();

    /**
     *
     * @see \LineMetrics\LM3\ObjectTypes\BaseObjectInterface::getObjectId()
     */
    public function getObjectId()
    {
        return $this->objectId;
    }

    /**
     * Sets object id
     *
     * @param $objectId string|null
     * @return BaseObject
     */
    public function setObjectId($objectId)
    {
        $this->objectId = $objectId;
        return $this;
    }

    /**
     *
     * @see \LineMetrics\LM3\ObjectTypes\BaseObjectInterface::getObjectType()
     */
    public function getObjectType()
    {
        return $this->objectType;
    }

    /**
     * Sets object type;
     *
     * @param $objectType string|null
     * @return BaseObject
     */
    public function setObjectType($objectType)
    {
        $this->objectType = $objectType;
        return $this;
    }

    /**
     *
     * @see \LineMetrics\LM3\ObjectTypes\BaseObjectInterface::getTemplateId()
     */
    public function getTemplateId()
    {
        return $this->templateId;
    }

    /**
     * Sets template id
     *
     * @param $templateId string|null
     *
     * @return BaseObject
     */
    public function setTemplateId($templateId)
    {
        $this->templateId = $templateId;
        return $this;
    }

    /**
     *
     * @see \LineMetrics\LM3\ObjectTypes\BaseObjectInterface::getParentId()
     */
    public function getParentId()
    {
        return $this->parentId;
    }

    /**
     * Sets parent id
     *
     * @param $parentId string|null
     * @return BaseObject
     */
    public function setParentId($parentId)
    {
        $this->parentId = $parentId;
        return $this;
    }

    /**
     *
     * @see \LineMetrics\LM3\ObjectTypes\BaseObjectInterface::getPayload()
     */
    public function getPayload()
    {
        return $this->payload;
    }

    /**
     * Sets payload
     *
     * @param $payload array
     * @return BaseObject
     */
    public function setPayload(array $payload)
    {
        $this->payload = $payload;
        return $this;
    }


    /**
     * @see \LineMetrics\LM3\ObjectTypes\BaseObjectInterface::fromJsonObject()
     * @return BaseObject
     */
    public function fromJsonObject( $jsonObject ){
        $this->setObjectId($jsonObject->object_id);
        $this->setObjectType($jsonObject->object_type);
        $this->setParentId($jsonObject->parent_id);
        $this->setTemplateId($jsonObject->template_id);

        if( isset( $jsonObject->payload) ){
            $this->setPayload((array) $jsonObject->payload);
        }
        else{
            $this->setPayload( array() );
        }

        return $this;
    }

}