<?php
namespace LineMetrics\LM3\ObjectTypes;
use \LineMetrics\LM3\ObjectTypes\DataFunctionsEnum;
use \LineMetrics\LM3\AliasAwareInterface;

interface DataStreamInterface extends BaseObjectInterface, AliasAwareInterface, TitleAwareInterface, IconAwareInterface
{
    /**
     * Gets id
     *
     * @return string
     */
    public function getId();

    /**
     * Updates datastream
     *
     * @return string
      */
    public function save();


    /**
     * Loads stream data
     *
     * @param integer|string $from Time from in ms
     * @param integer|string $to Time to in ms
     * @param string $timezone (OPTIONAL) Timezone identifier
     * @param string $granularity (OPTIONAL) Data granularity
     * @param string $function (OPTIONAL) Data function
     *
     * @return array
     */
    public function loadData( $from, $to, $timezone = null, $granularity = null, $function = DataFunctionsEnum::FUNCTION_DEFAULT);

    /*

    public function loadLastValue();



    public function saveData();

*/
}
