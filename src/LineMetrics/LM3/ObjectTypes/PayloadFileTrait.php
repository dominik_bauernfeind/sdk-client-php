<?php
namespace LineMetrics\LM3\ObjectTypes;

trait PayloadFileTrait
{
    /**
     * Sets file
     *
     * @param string|null $file
     * @return \LineMetrics\LM3\ObjectTypes\PayloadFileTrait
     */
    public function setFile($file)
    {
        if (! is_array($this->payload)) {
            $this->payload = array();
        }

        $this->payload['file'] = $file;
        return $this;
    }

     /**
     * Gets title
     *
     * @return string|null
     */
    public function getFile()
    {
        if (is_array($this->payload) && array_key_exists('file', $this->payload)) {
            return $this->payload['file'];
        } else {
            return null;
        }
    }
}