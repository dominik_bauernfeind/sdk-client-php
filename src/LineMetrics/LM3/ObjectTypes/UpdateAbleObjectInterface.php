<?php
namespace LineMetrics\LM3\ObjectTypes;

interface UpdateAbleObjectInterface extends BaseObjectInterface
{
    public function save();
}