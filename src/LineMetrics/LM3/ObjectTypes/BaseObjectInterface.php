<?php
namespace LineMetrics\LM3\ObjectTypes;

use  LineMetrics\LM3\Api\ApiAwareInterface;

interface BaseObjectInterface extends ApiAwareInterface
{
    /**
     * Gets object id
     *
     * @return string
     */
    public function getObjectId();

    /**
     * Gets object type
     *
     * @return string
     */
    public function getObjectType();

    /**
     * Gets template id
     *
     * @return string|null
     */
    public function getTemplateId();

    /**
     * Gets parent id
     *
     * @return string|null
     */
    public function getParentId();

     /**
     * Gets payload
     *
     * @return array
     */
    public function getPayload();

    /**
     * Populates object from a deserialized json represntation
     *
     * @param stdClass $object
     * @return BaseObjectInterface
     */
    public function fromJsonObject( $object );


}
