<?php
namespace LineMetrics\LM3\ObjectTypes;

use LineMetrics\LM3\DataTypes\BaseTypeInterface;
use LineMetrics\LM3\DataTypes\StringType;
use LineMetrics\LM3\DataTypes\SimpleDateType;
use LineMetrics\LM3\DataTypes\BooleanType;
use LineMetrics\LM3\DataTypes\DoubleType;
use LineMetrics\LM3\DataTypes\TableType;
use LineMetrics\LM3\DataTypes\GeoAddressType;
use LineMetrics\LM3\DataTypes\ConfigurationInterface;
use LineMetrics\LM3\RequestTypes\UpdateObjectRequest;
use LineMetrics\LM3\RequestTypes\ConfigRequest;
use LineMetrics\LM3\RequestTypes\WriteDataRequest;
use LineMetrics\LM3\DataTypes\DataTypesFactoryAwareTrait;
use LineMetrics\LM3\AliasAwareTrait;
use LineMetrics\LM3\Exception\UnexpectedValueException;
use LineMetrics\LM3\DataTypes\SimpleDateTypeInterface;

class Property extends BaseObject implements PropertyInterface
{

    use AliasAwareTrait;
    use PayloadIconTrait;
    use PayloadTitleTrait;
    use DataTypesFactoryAwareTrait;

    protected $value;

    protected $data;

    /**
     *
     * @see \LineMetrics\LM3\ObjectTypes\AssetInterface::getId()
     */
    public function getId()
    {
        if (! empty($this->parentId) && ! empty($this->alias)) {
            return $this->parentId . '/' . $this->alias;
        } else {
            return $this->objectId;
        }
    }

    /**
     *
     * @see \LineMetrics\LM3\ObjectTypes\PropertyInterface::getPropertyType()
     * @return Asset
     */
    public function getPropertyType()
    {
        if (is_array($this->payload) && array_key_exists('propertyType', $this->payload)) {
            return $this->payload['propertyType'];
        } else {
            return null;
        }
    }

    /**
     *
     * @see \LineMetrics\LM3\ObjectTypes\BaseObjectInterface::fromJsonObject()
     * @return Property
     */
    public function fromJsonObject($jsonObject)
    {
        parent::fromJsonObject($jsonObject);
        if (isset($jsonObject->alias)) {
            $this->setAlias($jsonObject->alias);
        }

        if (isset($jsonObject->data)) {
            $this->data = $jsonObject->data;
        }

        return $this;
    }

    public function getValue()
    {
        if (FALSE === $this->value instanceof BaseTypeInterface) {

            if(!isset( $this->data) || !isset($this->data->val)){
                return null;
            }

            switch ($this->getPropertyType()) {
                case 'simpleDate':
                    $this->value = new SimpleDateType($this->data->val, $this->data->ts / 1000);
                    break;

                case 'longtext':
                case 'shorttext':
                    $this->value = new StringType($this->data->val, $this->data->ts / 1000);
                    break;

                    break;

                case 'boolean':
                    $this->value = new BooleanType($this->data->val, $this->data->ts / 1000);
                    break;

                case 'address':
                    $this->value = new GeoAddressType($this->data->val, $this->data->lat, $this->data->long, $this->data->ts / 1000);
                    break;

                case 'table':
                    $this->value = new TableType($this->data->val);
                    break;  

                case '':
                    $this->value = new DoubleType($this->data->val, $this->data->ts / 1000);
                    break;

                default:
                    breaK;
            }

            return $this->value;
        } else {
            return $this->value;
        }
    }

    public function setValue(BaseTypeInterface $value, ConfigurationInterface $input = null)
    {
        if (! $this->api) {
            throw new UnexpectedValueException('No Api interface set');
        }

        if( $input ){
            if (! $this->typesFactory) {
                throw new UnexpectedValueException('No data types factory set');
            }

            $interfaceName = $this->typesFactory->getInterfaceNameFor($input->getInput());

            if( !is_a($value, $interfaceName)){
                throw new UnexpectedValueException('Invalid value provied. Got "' . $value->getTypeName() . '" expecting "'. $input->getInput() . '"' );
            }
        }

        $request = new WriteDataRequest();
        $request->setObjectId($this->getId());

        if( $value instanceof  SimpleDateTypeInterface ){
            $request->setEncodingFlags(JSON_NUMERIC_CHECK);
        }

        $request->setData( new \ArrayObject( array( $this->getId() => $value) ));
        return $this->api->writeData($request);
    }

    /**
     *
     * @see \LineMetrics\LM3\ObjectTypes\PropertyInterface::save()
     */
    public function save()
    {
        if (! $this->api) {
            throw new UnexpectedValueException('No Api interface set');
        }

        $request = new UpdateObjectRequest();
        $request->setObjectId($this->getId());
        $request->setName($this->getTitle());
        $request->setAlias($this->getAlias());
        return $this->api->updateObject($request);
    }

    /**
     *
     * @see \LineMetrics\LM3\ObjectTypes\PropertyInterface::loadConfig()
     */
    public function loadConfig()
    {
        if (! $this->api) {
            throw new UnexpectedValueException('No Api interface set');
        }

        $request = new ConfigRequest();
        $request->setObjectId($this->getId());
        return $this->api->loadConfig($request);
    }
}