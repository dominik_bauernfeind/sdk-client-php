<?php
namespace LineMetrics\LM3\ObjectTypes;

abstract class DataFunctionsEnum
{
    const FUNCTION_DEFAULT = 'default';

    const FUNCTION_RAW = 'raw';

    const FUNCTION_SUM = 'sum';

    const FUNCTION_AVG = 'average';

    const FUNCTION_LAST_VALUE = 'last_value';
}