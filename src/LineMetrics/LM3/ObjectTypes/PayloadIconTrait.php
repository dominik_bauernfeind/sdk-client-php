<?php
namespace LineMetrics\LM3\ObjectTypes;

trait PayloadIconTrait
{
    /**
     * Sets icon
     *
     * @param string|null $icon
     * @return \LineMetrics\LM3\ObjectTypes\PayloadIconTrait
     */
    public function setIcon($icon)
    {
        if (! is_array($this->payload)) {
            $this->payload = array();
        }

        $this->payload['icon'] = $icon;
        return $this;
    }

    /**
     * Gets icon
     *
     * @return string|null
     */
    public function getIcon()
    {
        if (is_array($this->payload) && array_key_exists('icon', $this->payload)) {
            return $this->payload['icon'];
        } else {
            return null;
        }
    }
}