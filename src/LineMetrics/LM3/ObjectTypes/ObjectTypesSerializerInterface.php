<?php
namespace LineMetrics\LM3\ObjectTypes;

interface ObjectTypesSerializerInterface
{
    /**
     * Creates an object from deserialized JSON
      *
     * @param stdClass $payload
     * @return BaseObjectInterface
     */
    public function unserialize(  \stdClass $payload );
}