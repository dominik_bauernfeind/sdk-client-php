<?php
namespace LineMetrics\LM3\ObjectTypes;

use LineMetrics\LM3\RequestTypes\UpdateObjectRequest;
use LineMetrics\LM3\RequestTypes\LoadDataRequest;
use LineMetrics\LM3\RequestTypes\LoadLastValueRequest;
use LineMetrics\LM3\AliasAwareTrait;
use LineMetrics\LM3\Exception\UnexpectedValueException;

class DataStream extends BaseObject implements DataStreamInterface
{

    use AliasAwareTrait;
    use PayloadIconTrait;
    use PayloadTitleTrait;

    public function getId()
    {
        if (! empty($this->parentId) && ! empty($this->alias)) {
            return $this->parentId . '/' . $this->alias;
        } else {
            return $this->objectId;
        }
    }

    /**
     *
     * @see \LineMetrics\LM3\ObjectTypes\DataStreamInterface::save()
     */
    public function save()
    {
        if (! $this->api) {
            throw new UnexpectedValueException('No Api interface set');
        }

        $request = new UpdateObjectRequest();
        $request->setObjectId($this->getId());
        $request->setName($this->getTitle());
        $request->setAlias($this->getAlias());
        return $this->api->updateObject($request);
    }

    /**
     *
     * @see \LineMetrics\LM3\ObjectTypes\BaseObjectInterface::fromJsonObject()
     * @return Property
     */
    public function fromJsonObject($jsonObject)
    {
        parent::fromJsonObject($jsonObject);
        if (isset($jsonObject->alias)) {
            $this->setAlias($jsonObject->alias);
        }

        if (isset($jsonObject->data)) {
            $this->data = $jsonObject->data;
        }

        return $this;
    }

    /**
     * @see \LineMetrics\LM3\ObjectTypes\DataStreamInterface::loadData()
     */
    public function loadData($from, $to, $timezone = null, $granularity = null, $function = DataFunctionsEnum::FUNCTION_DEFAULT)
    {
         if (! $this->api) {
            throw new UnexpectedValueException('No Api interface set');
        }

        $request = new LoadDataRequest();
        $request->setObjectId($this->getId());
        $request->setFrom($from);
        $request->setTo($to);
        $request->setTimezone($timezone);
        $request->setGranularity($granularity);
        $request->setFunction($function);
        return $this->api->loadData($request);
    }

    /**
     * @see \LineMetrics\LM3\ObjectTypes\DataStreamInterface::loadLastValue()
     */
    public function loadLastValue()
    {
         if (! $this->api) {
            throw new UnexpectedValueException('No Api interface set');
        }

        $request = new LoadLastValueRequest();
        $request->setObjectId($this->getId());
        return $this->api->loadLastValue($request);
    }


}
