<?php
namespace LineMetrics\LM3\ObjectTypes;

interface DocumentInterface extends BaseObjectInterface, UpdateAbleObjectInterface, TitleAwareInterface, FileAwareInterface
{

}