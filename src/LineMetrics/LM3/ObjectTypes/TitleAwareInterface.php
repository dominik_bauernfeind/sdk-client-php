<?php
namespace LineMetrics\LM3\ObjectTypes;

interface TitleAwareInterface
{
    /**
     * Sets title
     *
     * @param string|null $title
     * @return TitleAwareInterface
     */
    public function setTitle($title);

    /**
     * Gets title
     *
     * @return string|null
     */
    public function getTitle();
}