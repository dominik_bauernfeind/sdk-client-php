<?php
namespace LineMetrics\LM3\ObjectTypes;

interface IconAwareInterface
{
    /**
     * Sets icon
     *
     * @param string|null $alias
     * @return IconAwareInterface
     */
    public function setIcon($alias);

    /**
     * Gets icon
     *
     * @return string|null
     */
    public function getIcon();
}