<?php
namespace LineMetrics\LM3\ObjectTypes;

use LineMetrics\LM3\Collection\BaseCollectionInterface;

interface ObjectTypesCollectionInterface extends BaseCollectionInterface, \LineMetrics\LM3\Api\ApiAwareInterface, \LineMetrics\LM3\DataTypes\DataTypesFactoryAwareInterface
{

    /**
     * Returns first item
     *
     * @return BaseObjectInterface|null
     */
    public function first();

     /**
     * Returns last item
     *
     * @return BaseObjectInterface|null
     */
    public function last();
}

