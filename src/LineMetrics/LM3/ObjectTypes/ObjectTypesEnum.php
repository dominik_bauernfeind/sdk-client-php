<?php
namespace LineMetrics\LM3\ObjectTypes;

abstract class ObjectTypesEnum
{
    const OBJECT_TYPE_ASSET = "object";

    const OBJECT_TYPE_PROPERTY = "property";

    const OBJECT_TYPE_DATASTREAM = "attribute";

    const OBJECT_TYPE_DOCUMENT = "document";
}
