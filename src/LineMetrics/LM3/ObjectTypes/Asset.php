<?php
namespace LineMetrics\LM3\ObjectTypes;

use LineMetrics\LM3\RequestTypes\AssetRequest;
use LineMetrics\LM3\RequestTypes\UpdateObjectRequest;
use LineMetrics\LM3\CustomKeyAwareTrait;
use LineMetrics\LM3\Exception\UnexpectedValueException;

class Asset extends BaseObject implements AssetInterface
{

    use PayloadIconTrait;
    use PayloadTitleTrait;
    use CustomKeyAwareTrait;

    /**
     * Array of children info
     *
     * @var array
     */
    protected $childrenInfo = array();


    /**
     * Gets children info
     *
     * @return array
     */
    public function getChildrenInfo()
    {
        return $this->childrenInfo;
    }

    /**
     * Sets children info
     *
     * @param $childrenInfo array
     * @return Asset
     */
    public function setChildrenInfo(array $childrenInfo)
    {
        $this->childrenInfo = $childrenInfo;
        return $this;
    }

    /**
     *
     * @see \LineMetrics\LM3\ObjectTypes\AssetInterface::getId()
     */
    public function getId()
    {
        if (! empty($this->customKey)) {
            return $this->customKey;
        } else {
            return $this->objectId;
        }
    }


    /**
     *
     * @see \LineMetrics\LM3\ObjectTypes\AssetInterface::getImage()
     */
    public function getImage()
    {
        if (is_array($this->payload) && array_key_exists('image', $this->payload)) {
            return $this->payload['image'];
        } else {
            return null;
        }
    }

    /**
     *
     * @see \LineMetrics\LM3\ObjectTypes\BaseObjectInterface::fromJsonObject()
     * @return Asset
     */
    public function fromJsonObject($jsonObject)
    {
        parent::fromJsonObject($jsonObject);
        $this->setCustomKey($jsonObject->custom_key);
        return $this;
    }

    /**
     *
     * @see \LineMetrics\LM3\ObjectTypes\AssetInterface::save()
     */
    public function save()
    {
        if (! $this->api) {
            throw new UnexpectedValueException('No Api interface set');
        }

        $request = new UpdateObjectRequest();
        $request->setObjectId($this->getId());
        $request->setName($this->getTitle());
        $request->setCustomKey($this->getCustomKey());
        return $this->api->updateAsset($request);
    }

    /**
     *
     * @see \LineMetrics\LM3\ObjectTypes\AssetInterface::loadAssets()
     */
    public function loadAssets()
    {
        if (! $this->api) {
            throw new UnexpectedValueException('No Api interface set');
        }

        $request = new AssetRequest();
        $request->setObjectId($this->getId());
        return $this->api->loadAssets($request);
    }

    /**
     *
     * @see \LineMetrics\LM3\ObjectTypes\AssetInterface::loadProperties()
     */
    public function loadProperties()
    {
        if (! $this->api) {
            throw new UnexpectedValueException('No Api interface set');
        }

        $request = new AssetRequest();
        $request->setObjectId($this->getId());
        $request->setObjectType(ObjectTypesEnum::OBJECT_TYPE_PROPERTY);
        $request->setLoadPropertyData(true);
        return $this->api->loadAssets($request);
    }

    /**
     *
     * @see \LineMetrics\LM3\ObjectTypes\AssetInterface::loadDataStreams()
     */
    public function loadDataStreams()
    {
        if (! $this->api) {
            throw new UnexpectedValueException('No Api interface set');
        }

        $request = new AssetRequest();
        $request->setObjectId($this->getId());
        $request->setObjectType(ObjectTypesEnum::OBJECT_TYPE_DATASTREAM);
        return $this->api->loadAssets($request);
    }

    /**
     *
     * @see \LineMetrics\LM3\ObjectTypes\AssetInterface::loadDocuments()
     */
    public function loadDocuments()
    {
        if (! $this->api) {
            throw new UnexpectedValueException('No Api interface set');
        }

        $request = new AssetRequest();
        $request->setObjectId($this->getId());
        $request->setObjectType(ObjectTypesEnum::OBJECT_TYPE_DOCUMENT);
        return $this->api->loadAssets($request);
    }
}
