<?php
namespace LineMetrics\LM3\ObjectTypes;

use LineMetrics\LM3\CustomKeyAwareInterface;

interface AssetInterface extends BaseObjectInterface, CustomKeyAwareInterface, UpdateAbleObjectInterface, TitleAwareInterface, IconAwareInterface
{

    /**
     * Gets id
     *
     * If custom key is present the custom key will be returned
     *
     * @return string
     */
    public function getId();


    /**
     * Gets image
     *
     * @return string|null
     */
    public function getImage();


    /**
     * Gets children info
     *
     * @return array
     */
    public function getChildrenInfo();

    /**
     * Loads child assets
     *
     * @return ObjectTypesCollectionInterface
     */
    public function loadAssets();

    /**
     * Loads properties
     *
     * @return ObjectTypesCollectionInterface
     */
    public function loadProperties();

    /**
     * Loads data streams
     *
     * @return ObjectTypesCollectionInterface
     */
    public function loadDataStreams();


    /**
     * Loads data streams
     *
     * @return ObjectTypesCollectionInterface
     */
    public function loadDocuments();

}
