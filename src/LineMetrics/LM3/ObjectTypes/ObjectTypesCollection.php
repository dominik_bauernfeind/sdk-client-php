<?php
namespace LineMetrics\LM3\ObjectTypes;

use LineMetrics\LM3\ObjectTypes\ObjectSerializerInterface;
use LineMetrics\LM3\Api\ApiAwareInterface;
use LineMetrics\LM3\DataTypes\DataTypesFactoryInterface;
use LineMetrics\LM3\DataTypes\DataTypesFactoryAwareInterface;
use LineMetrics\LM3\Collection\BaseCollection;
use LineMetrics\LM3\AliasAwareInterface;
use LineMetrics\LM3\CustomKeyAwareInterface;

class ObjectTypesCollection extends BaseCollection implements ObjectTypesCollectionInterface
{
    /**
     * Object serializer
     *
     * @var ObjectSerializerInterface
     */
    protected $serializer = null;

    /**
     * Api reference
     *
     * @var ApiAwareInterface
     */
    protected $api = null;

    /**
     * Data types factory
     *
     * @var DataTypesFactoryInterface
     */
    protected $typesFactory = null;

    public function __construct(array $items)
    {
        $this->items = $items;
        parent::__construct($items, new ObjectTypesContract(new ObjectTypesSerializer()));
    }

    /**
     *
     * @see Countable::count()
     */
    public function count()
    {
        return count($this->items);
    }

    /**
     *
     * @see \LineMetrics\LM3\Api\ApiAwareInterface::setApi()
     * @return ObjectCollection
     */
    public function setApi(\LineMetrics\LM3\Api\ApiInterface $api = null)
    {
        $this->api = $api;
        return $this;
    }

    /**
     *
     * @see \LineMetrics\LM3\Api\ApiAwareInterface::getApi()
     */
    public function getApi()
    {
        return $this->api;
    }

    /**
     *
     * @see \LineMetrics\LM3\DataTypes\DataTypesFactoryAwareInterface::setDataTypesFactory()
     * @return ObjectCollection
     */
    public function setDataTypesFactory(\LineMetrics\LM3\DataTypes\DataTypesFactoryInterface $factory = null)
    {
        $this->typesFactory = $factory;
        return $this;
    }

    /**
     *
     * @see \LineMetrics\LM3\DataTypes\DataTypesFactoryAwareInterface::getDataTypesFactory()
     */
    public function getDataTypesFactory()
    {
        return $this->typesFactory;
    }

    /**
     *
     * @see \LineMetrics\LM3\Collection\BaseCollection::initItem()
     */
    protected function initItem($value)
    {
        if ($value instanceof ApiAwareInterface) {
            $value->setApi($this->api);
        }

        if ($value instanceof DataTypesFactoryAwareInterface) {
            $value->setDataTypesFactory($this->typesFactory);
        }
    }

    public function findOneByAlias($alias)
    {
        $r = array_filter($this->items, function ($x) use($alias)
        {
            if ($x instanceof AliasAwareInterface) {
                return $x->getAlias() == $alias;
            } else
                if ($x instanceof \stdClass && isset( $x->alias ) ) {
                    return $x->alias == $alias;
                }

            return false;
        });

        if (count($r)) {
            $index = key($r);
            return $this->offsetGet($index);
        } else {
            return false;
        }
    }

    public function findOneByCustomKey($customKey)
    {
        $r = array_filter($this->items, function ($x) use($customKey)
        {
            if ($x instanceof CustomKeyAwareInterface) {
                return $x->getCustomKey() == $customKey;
            } else
                if ($x instanceof \stdClass && isset( $x->custom_key ) ) {
                    return $x->custom_key == $customKey;
                }

            return false;
        });

        if (count($r)) {
            $index = key($r);
            return $this->offsetGet($index);
        } else {
            return false;
        }
    }
}
