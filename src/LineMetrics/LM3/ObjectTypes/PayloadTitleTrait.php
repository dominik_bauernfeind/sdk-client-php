<?php
namespace LineMetrics\LM3\ObjectTypes;

trait PayloadTitleTrait
{
   /**
    * Sets title
    *
    * @param string|null $title
    * @return \LineMetrics\LM3\ObjectTypes\PayloadTitleTrait
    */
    public function setTitle($title)
    {
        if (! is_array($this->payload)) {
            $this->payload = array();
        }

        $this->payload['title'] = $title;
        return $this;
    }

    /**
     * Gets title
     *
     * @return string|null
     */
    public function getTitle()
    {
        if (is_array($this->payload) && array_key_exists('title', $this->payload)) {
            return $this->payload['title'];
        } else {
            return null;
        }
    }
}