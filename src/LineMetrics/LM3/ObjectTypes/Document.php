<?php
namespace LineMetrics\LM3\ObjectTypes;

use LineMetrics\LM3\RequestTypes\UpdateObjectRequest;
use LineMetrics\LM3\Exception\UnexpectedValueException;

class Document extends BaseObject implements DocumentInterface
{
    use PayloadTitleTrait;
    use PayloadFileTrait;

    public function getFileUrl()
    {
        return "https://cloud.linemetrics.com/file/" . $this->getFile();
    }

    /**
     * @see \LineMetrics\LM3\ObjectTypes\UpdateAbleObjectInterface::save()
     */
    public function save()
    {
        if (! $this->api) {
            throw new UnexpectedValueException('No Api interface set');
        }

        $request = new UpdateObjectRequest();
        $request->setObjectId($this->getId());
        $request->setName($this->getTitle());
        return $this->api->updateAsset($request);
    }
}
