<?php
namespace LineMetrics\LM3\ObjectTypes;

interface FileAwareInterface
{
    /**
     * Sets file
     *
     * @param string|null $file
     * @return IconAwareInterface
     */
    public function setFile($file);

    /**
     * Gets file
     *
     * @return string|null
     */
    public function getFile();
}