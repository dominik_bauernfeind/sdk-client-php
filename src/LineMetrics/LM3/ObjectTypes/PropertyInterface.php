<?php
namespace LineMetrics\LM3\ObjectTypes;

use \LineMetrics\LM3\DataTypes\ConfigurationInterface;
use \LineMetrics\LM3\DataTypes\DataTypesFactoryAwareInterface;
use \LineMetrics\LM3\AliasAwareInterface;

interface PropertyInterface extends
        BaseObjectInterface,
        UpdateAbleObjectInterface,
        AliasAwareInterface,
        TitleAwareInterface,
        IconAwareInterface,
        DataTypesFactoryAwareInterface
{

    /**
     * Gets property type
     *
     * @return string
     */
    public function getPropertyType();

    /**
     * Loads property data type configuration
     *
     * @return ConfigurationInterface
     */
    public function loadConfig();
}
