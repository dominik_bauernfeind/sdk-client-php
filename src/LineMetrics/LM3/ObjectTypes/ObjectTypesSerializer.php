<?php
namespace LineMetrics\LM3\ObjectTypes;
use LineMetrics\LM3\ObjectTypes\Asset;
use LineMetrics\LM3\ObjectTypes\Property;
use LineMetrics\LM3\ObjectTypes\DataStream;
use LineMetrics\LM3\ObjectTypes\Document;
use LineMetrics\LM3\Exception\InvalidArgumentException;
use LineMetrics\LM3\Exception\UnexpectedValueException;

class ObjectTypesSerializer implements ObjectTypesSerializerInterface
{

    /**
     * @see \LineMetrics\LM3\ObjectTypes\ObjectTypesSerializerInterface::unserialize()
     */
    public function unserialize( \stdClass $payload)
    {
        if( !isset( $payload->object_type ) ){
            throw new InvalidArgumentException('Property "payload->object_type" not set');
        }

        $object = null;

        switch ($payload->object_type) {
            case "object":
                $object = $this->createAsset();
                $object->fromJsonObject($payload);
                break;

            case "property":
                $object = $this->createProperty();
                $object->fromJsonObject($payload);
                break;

            case "attribute":
                $object = $this->createDataStream();
                $object->fromJsonObject($payload);
                break;

            case "document":
                $object = $this->createDocument();
                $object->fromJsonObject($payload);
                break;

            default:
                throw new UnexpectedValueException('Invalid object type "' . $payload->object_type . '"');
                break;
        }

        return $object;
    }

    protected function createAsset()
    {
        return new Asset();
    }

    protected function createProperty()
    {
        return new Property();
    }

    protected function createDataStream()
    {
        return new DataStream();
    }

    protected function createDocument()
    {
        return new Document();
    }
}