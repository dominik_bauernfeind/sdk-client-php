<?php
namespace LineMetrics\LM3\Connector;
use LineMetrics\LM3\Auth\AuthTokenInterface;
use LineMetrics\LM3\RequestTypes\AssetRequestInterface;
use LineMetrics\LM3\RequestTypes\UpdateObjectRequestInterface;
use LineMetrics\LM3\RequestTypes\ObjectRequestInterface;
use LineMetrics\LM3\RequestTypes\ConfigRequestInterface;
use LineMetrics\LM3\RequestTypes\LoadDataRequestInterface;
use LineMetrics\LM3\RequestTypes\LoadLastValueRequestInterface;
use LineMetrics\LM3\RequestTypes\WriteDataRequestInterface;
use LineMetrics\LM3\RequestTypes\TemplatesRequestInterface;
use LineMetrics\LM3\RequestTypes\RequiredFieldsRequestInterface;
use LineMetrics\LM3\DataTypes\ConfigurationInterface;
use LineMetrics\LM3\DataTypes\DataTypesCollectionInterface;
use LineMetrics\LM3\ObjectTypes\ObjectTypesCollectionInterface;
use LineMetrics\LM3\Templates\TemplatesCollectionInterface;

interface ConnectorInterface
{

    /**
     * Loads an asset
     *
     * @param AuthTokenInterface $accessToken The access token
     * @param ObjectRequestInterface The object request
     * @return BaseObjectInterface
     */
    public function load( AuthTokenInterface $accessToken, ObjectRequestInterface $request);

    /**
     * Loads child assets
     *
     * @param AuthTokenInterface $accessToken The access token
     * @param AssetRequestInterface  $request ( OPTIONAL ) The asset request. Pass null to load root assets
     * @return ObjectTypesCollectionInterface
     */
    public function loadAssets( AuthTokenInterface $accessToken, AssetRequestInterface $request = null);


    /**
     * Updates an object
     *
     * @param AuthTokenInterface $accessToken The access token
     * @param UpdateObjectRequestInterface $request The request
     * @return string
     */
    public function updateObject( AuthTokenInterface $accessToken, UpdateObjectRequestInterface $request);


    /**
     * Loads input and output data type configuration
     *
     * @param AuthTokenInterface $accessToken The access token
     * @param ConfigRequestInterface $request The request
     * @return ConfigurationInterface
     */
    public function loadConfig( AuthTokenInterface $accessToken, ConfigRequestInterface $request);


    /**
     * Loads data
     *
     * If $output is set the connector tries to return a typed collection
     * based on the ouput configuration. If it not possible a generic
     * data collection will be returned.
     *
     * @param AuthTokenInterface $accessToken The access token
     * @param LoadDataRequestInterface $request The request
     * @param ConfigurationInterface $output ( OPTIONAL ) Output type configuration
     * @return DataTypesCollectionInterface
     */
    public function loadData( AuthTokenInterface $accessToken, LoadDataRequestInterface $request, ConfigurationInterface $output = null);

     /**
     * Loads last value
     *
     * If $output is set the connector tries to return a subclass instance
     * of BaseTypeInterface. If it not possible it returns the raw output in
     * form of stdClass.
     *
     * @param AuthTokenInterface $accessToken The access token
     * @param LoadLastValueRequestInterface $request The request
     * @param ConfigurationInterface $output ( OPTIONAL ) Output type configuration
     * @return BaseTypeInterface|\stdClass
     */
    public function loadLastValue( AuthTokenInterface $accessToken, LoadLastValueRequestInterface $request, ConfigurationInterface $ouput = null);


    /**
     * Write data
     *
     * @param AuthTokenInterface $accessToken The access token
     * @param WriteDataRequestInterface $request The request
     */
    public function writeData( AuthTokenInterface $accessToken, WriteDataRequestInterface $request );



    /**
     * Write data
     *
     * @param AuthTokenInterface $accessToken The access token
     * @param TemplatesRequestInterface $request ( OPTIONAL ) The request
     * @return TemplatesCollectionInterface
     */
    public function loadTemplates( AuthTokenInterface $accessToken, TemplatesRequestInterface  $request = null );



    /**
     * Loads required fields from template
     *
     * @param AuthTokenInterface $accessToken The access token
     * @param RequiredFieldsRequestInterface $request ( OPTIONAL ) The request
     * @return array
     */
    public function loadRequiredFields(  AuthTokenInterface $accessToken, RequiredFieldsRequestInterface  $request);

}