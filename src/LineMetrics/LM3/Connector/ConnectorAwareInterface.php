<?php
namespace LineMetrics\LM3\Connector;

interface ConnectorAwareInterface
{
   public function setConnector( ConnectorInterface $connector);
}