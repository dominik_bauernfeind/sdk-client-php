<?php
namespace LineMetrics\LM3\Connector;

trait ConnectorAwareTrait
{
    protected $connector;

    public function setConnector( ConnectorInterface $connector = null ){
        $this->connector = $connector;
        return  $this;
    }
}