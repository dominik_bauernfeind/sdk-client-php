<?php
namespace LineMetrics\LM3\Connector;

use LineMetrics\LM3\Exception\LM3ExceptionInterface;

class ConnectorException extends \Exception implements LM3ExceptionInterface
{


}
