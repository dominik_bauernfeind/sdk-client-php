<?php
namespace LineMetrics\LM3;

use LineMetrics\LM3\Api\ApiInterface;
use LineMetrics\LM3\Api\ApiException;
use LineMetrics\LM3\Auth\AuthenticatorInterface;
use LineMetrics\LM3\Connector\ConnectorInterface;
use LineMetrics\LM3\RequestTypes\AssetRequestInterface;
use LineMetrics\LM3\RequestTypes\UpdateObjectRequestInterface;
use LineMetrics\LM3\RequestTypes\ObjectRequestInterface;
use LineMetrics\LM3\RequestTypes\ConfigRequestInterface;
use LineMetrics\LM3\RequestTypes\LoadDataRequestInterface;
use LineMetrics\LM3\RequestTypes\LoadLastValueRequestInterface;
use LineMetrics\LM3\DataTypes\ConfigurationInterface;
use LineMetrics\LM3\Api\ApiAwareInterface;
use LineMetrics\LM3\DataTypes\DataTypesFactoryAwareInterface;
use LineMetrics\LM3\DataTypes\DataTypesFactoryInterface;
use LineMetrics\LM3\Exception\LM3ExceptionInterface;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerInterface;
use Psr\Log\LoggerAwareTrait;
use Psr\Cache\CacheItemPoolInterface;
use LineMetrics\LM3\Connector\ConnectorAwareInterface;
use LineMetrics\LM3\Connector\ConnectorAwareTrait;
use LineMetrics\LM3\Auth\AuthenticatorAwareInterface;
use LineMetrics\LM3\Auth\AuthenticatorAwareTrait;
use LineMetrics\LM3\DataTypes\DataTypesFactoryAwareTrait;
use LineMetrics\LM3\Auth\AuthTokenCacheAwareInterface;
use LineMetrics\LM3\Auth\AuthTokenCacheAwareTrait;
use Cache\Adapter\PHPArray\ArrayCachePool;

class Api implements ApiInterface, LoggerAwareInterface, ConnectorAwareInterface, AuthenticatorAwareInterface, DataTypesFactoryAwareInterface, AuthTokenCacheAwareInterface
{
    use LoggerAwareTrait;
    use ConnectorAwareTrait;
    use AuthenticatorAwareTrait;
    use DataTypesFactoryAwareTrait;
    use AuthTokenCacheAwareTrait;

    protected $authTokenCacheKey;

    /**
     * Constructs new Api instance
     *
     * @param AuthenticatorInterface $auth ( OPTIONAL ) Authenticator instance
     * @param ConnectorInterface $connector ( OPTIONAL ) Connector instance
     * @param LoggerInterface $logger ( OPTIONAL ) Logger instance
     * @param DataTypesFactoryInterface $typesFactory ( OPTIONAL ) Data types factory instance
     * @param CacheItemPoolInterface $tokenCache ( OPTIONAL ) Auth token cache
     */
    function __construct(
        AuthenticatorInterface $auth = null,
        ConnectorInterface $connector= null,
        LoggerInterface $logger= null,
        DataTypesFactoryInterface $typesFactory = null,
        CacheItemPoolInterface $tokenCache = null,
        $tokenKey  = "lm3.accessToken" )
    {
       if( $auth){
           $this->setAuthenticator( $auth );
       }

       if( $connector){
           $this->setConnector($connector);
       }

       if( $logger ){
           $this->setLogger($logger);
       }

       if( $typesFactory ){
           $this->setDataTypesFactory($typesFactory);
       }

       if( $tokenCache ){
           $this->setAuthTokenCache($tokenCache);
       }
       else{
           $this->setAuthTokenCache(new ArrayCachePool());
       }

       $this->authTokenCacheKey = $tokenKey;
    }


    /**
     * Creates data types factory
     *
     * @return DataTypesFactoryInterface
     * @throws ApiException Creation of factory class failed
     * @throws ConnectorInterface Class name of factory class is empty
     */
    protected function typesFactory()
    {
        if(!$this->typesFactory){
            throw new ApiException('No data types factory set');
        }

        return $this->typesFactory;
    }

    protected function checkToken()
    {
        if (! $this->token || $this->token->isExpired()) {
            return false;
        }

        return true;
    }

    protected function auth()
    {
        if(!$this->authenticator){
            throw new ApiException('No authenticator set');
        }

        if( $this->authTokenCache)
        {
            if( empty($this->authTokenCacheKey)){
                throw new ApiException('No cache key set for auth token');
            }

            $token = $this->authTokenCache->getItem($this->authTokenCacheKey);

            if( $this->logger){
                $this->logger->info("Api.auth Try getting token from cache");
            }

            if( $token->isHit() ){
                if( $this->logger){
                    $this->logger->info("Api.auth Cache hit found token", array("token"=>$token->get()));
                }
                return $token->get();
            }
            else{
                $token = $this->authenticator->auth();

                if( $this->logger){
                    $this->logger->info("Api.auth Cache miss. Getting new token", array("token"=>$token));
                }

                $item = $this->authTokenCache->getItem($this->authTokenCacheKey);
                $item->expiresAfter($token->getExpires() - 100 );
                $item->set($token);
                $this->authTokenCache->save($item);

                if( $this->logger){
                    $this->logger->info("Api.auth Saving token into cache");
                }

                return $token;
            }
        }
        else{
            $token = $this->authenticator->auth();

            if( $this->logger){
                $this->logger->info("Api.auth No auth token cache. Getting new token", array("token"=>$token));
            }

            return $token;
        }
    }

    protected function connect()
    {
        if(!$this->connector){
            throw new ApiException('No connector set');
        }

       return $this->connector;
    }


    public function logout()
    {
        if( $this->authTokenCache)
        {
            if( empty($this->authTokenCacheKey)){
                throw new ApiException('No cache key set for auth token');
            }

            $token = $this->authTokenCache->deleteItem($this->authTokenCacheKey);

            if( $this->logger){
                $this->logger->info("Api.auth Logging out from API. Removing token from cache");
            }
        }
    }

    /**
     *
     * @see \LineMetrics\LM3\Api\ApiInterface::load()
     * @throws LM3ExceptionInterface
     */
    public function load(ObjectRequestInterface $request)
    {
        \PHP_Timer::start();

        try{
            if( $this->logger){
                $this->logger->info("Api.load start");
                $this->logger->debug("Api.load arguments", array( '$request'=> $request ) );
            }

            $asset = $this->connect()->load( $this->auth(), $request);

            if ($asset instanceof ApiAwareInterface) {
                $asset->setApi($this);
            }

            if ($asset instanceof DataTypesFactoryAwareInterface) {
                $asset->setDataTypesFactory($this->typesFactory());
            }

            return $asset;
        }
        catch( \Exception $e ){
            if( $this->logger){
                $this->logger->error("Api.load exception", array('exception'=>$e));
            }

            throw $e;
        }
        finally {
            $time = \PHP_Timer::stop();
            if( $this->logger){
                $this->logger->info("Api.load end", array('took'=>\PHP_Timer::secondsToTimeString($time)));
            }
        }
    }

    /**
     *
     * @see \LineMetrics\LM3\Api\ApiInterface::loadAssets()
     * @throws LM3ExceptionInterface
     */
    public function loadAssets(AssetRequestInterface $request = null)
    {
        \PHP_Timer::start();

        try{
            if( $this->logger){
                $this->logger->info("Api.loadAssets start");
                $this->logger->debug("Api.loadAssets arguments", array( '$request'=> $request ) );
            }

            $assets = $this->connect()->loadAssets($this->auth(), $request);

            if ($assets instanceof ApiAwareInterface) {
                $assets->setApi($this);
            }

            if ($assets instanceof DataTypesFactoryAwareInterface) {
                $assets->setDataTypesFactory($this->typesFactory());
            }

            return $assets;
        }
        catch( \Exception $e ){
            if( $this->logger){
                $this->logger->error("Api.loadAssets exception", array('exception'=>$e));
            }

            throw $e;
        }
        finally {
            $time = \PHP_Timer::stop();
            if( $this->logger){
                $this->logger->info("Api.loadAssets end", array('took'=>\PHP_Timer::secondsToTimeString($time)));
            }
        }
    }

    /**
     *
     * @see \LineMetrics\LM3\Api\ApiInterface::updateObject()
     * @throws LM3ExceptionInterface
     */
    public function updateObject(UpdateObjectRequestInterface $request)
    {
        \PHP_Timer::start();

        try{
            if( $this->logger){
                $this->logger->info("Api.updateObject start");
                $this->logger->debug("Api.updateObject arguments", array( '$request'=> $request ) );
            }

            return $this->connect()->updateObject(  $this->auth(), $request);
        }
        catch( \Exception $e ){
            if( $this->logger){
                $this->logger->error("Api.updateObject exception", array('exception'=>$e));
            }

            throw $e;
        }
        finally {
            $time = \PHP_Timer::stop();
            if( $this->logger){
                $this->logger->info("Api.updateObject end", array('took'=>\PHP_Timer::secondsToTimeString($time)));
            }
        }
    }

    /**
     *
     * @see \LineMetrics\LM3\Api\ApiInterface::loadConfig()
     * @throws LM3ExceptionInterface
     */
    public function loadConfig(ConfigRequestInterface $request)
    {
        \PHP_Timer::start();

        try{
            if( $this->logger){
                $this->logger->info("Api.loadConfig start");
                $this->logger->debug("Api.loadConfig arguments", array( '$request'=> $request ) );
            }

            return $this->connect()->loadConfig($this->auth(), $request);
        }
        catch( \Exception $e ){
            if( $this->logger){
                $this->logger->error("Api.loadConfig exception", array('exception'=>$e));
            }

            throw $e;
        }
        finally {
            $time = \PHP_Timer::stop();
            if( $this->logger){
                $this->logger->info("Api.loadConfig end", array('took'=>\PHP_Timer::secondsToTimeString($time)));
            }
        }
    }

    /**
     *
     * @see \LineMetrics\LM3\Api\ApiInterface::loadData()
     * @throws LM3ExceptionInterface
     */
    public function loadData(LoadDataRequestInterface $request, ConfigurationInterface $ouput = null)
    {
        \PHP_Timer::start();

        try{
            if( $this->logger){
                $this->logger->info("Api.loadData start");
                $this->logger->debug("Api.loadData arguments", array( '$request'=> $request ) );
            }

            return $this->connect()->loadData($this->auth(), $request, $ouput);
        }
        catch( \Exception $e ){
            if( $this->logger){
                $this->logger->error("Api.loadData exception", array('exception'=>$e));
            }

            throw $e;
        }
        finally {
            $time = \PHP_Timer::stop();
            if( $this->logger){
                $this->logger->info("Api.loadData end", array('took'=>\PHP_Timer::secondsToTimeString($time)));
            }
        }
    }

    /**
     *
     * @see \LineMetrics\LM3\Api\ApiInterface::loadLastValue()
     * @throws LM3ExceptionInterface
     */
    public function loadLastValue(LoadLastValueRequestInterface $request, ConfigurationInterface $ouput = null)
    {
        \PHP_Timer::start();

        try{
            if( $this->logger){
                $this->logger->info("Api.loadLastValue start");
                $this->logger->debug("Api.loadLastValue arguments", array( '$request'=> $request ) );
            }

            return $this->connect()->loadLastValue($this->auth(), $request, $ouput);
        }
        catch( \Exception $e ){
            if( $this->logger){
                $this->logger->error("Api.loadLastValue exception", array('exception'=>$e));
            }

            throw $e;
        }
        finally {
            $time = \PHP_Timer::stop();
            if( $this->logger){
                $this->logger->info("Api.loadLastValue end", array('took'=>\PHP_Timer::secondsToTimeString($time)));
            }
        }
    }

    /**
     *
     * @see \LineMetrics\LM3\Api\ApiInterface::writeData()
     * @throws LM3ExceptionInterface
     */
    public function writeData(\LineMetrics\LM3\RequestTypes\WriteDataRequestInterface $request)
    {
        \PHP_Timer::start();

        try{
            if( $this->logger){
                $this->logger->info("Api.writeData start");
                $this->logger->debug("Api.writeData arguments", array( '$request'=> $request ) );
            }

            return $this->connect()->writeData($this->auth(), $request);
        }
        catch( \Exception $e ){
            if( $this->logger){
                $this->logger->error("Api.writeData exception", array('exception'=>$e));
            }

            throw $e;
        }
        finally {
            $time = \PHP_Timer::stop();
            if( $this->logger){
                $this->logger->info("Api.writeData end", array('took'=>\PHP_Timer::secondsToTimeString($time)));
            }
        }
    }

    /**
     *
     * @see \LineMetrics\LM3\Api\ApiInterface::loadTemplates()
     * @throws LM3ExceptionInterface
     */
    public function loadTemplates(\LineMetrics\LM3\RequestTypes\TemplatesRequestInterface $request = null)
    {
        \PHP_Timer::start();

        try{
            if( $this->logger){
                $this->logger->info("Api.loadTemplates start");
                $this->logger->debug("Api.loadTemplates arguments", array( '$request'=> $request ) );
            }

            $templates = $this->connect()->loadTemplates($this->auth(), $request);

            if ($templates instanceof ApiAwareInterface) {
                $templates->setApi($this);
            }

            if ($templates instanceof DataTypesFactoryAwareInterface) {
                $templates->setDataTypesFactory($this->typesFactory());
            }

            return $templates;
        }
        catch( \Exception $e ){
            if( $this->logger){
                $this->logger->error("Api.loadTemplates exception", array('exception'=>$e));
            }

            throw $e;
        }
        finally {
            $time = \PHP_Timer::stop();
            if( $this->logger){
                $this->logger->info("Api.loadTemplates end", array('took'=>\PHP_Timer::secondsToTimeString($time)));
            }
        }
    }

    /**
     *
     * @see \LineMetrics\LM3\Api\ApiInterface::loadRequiredFields()
     * @throws LM3ExceptionInterface
     */
    public function loadRequiredFields(\LineMetrics\LM3\RequestTypes\RequiredFieldsRequestInterface $request)
    {
        \PHP_Timer::start();

        try{
            if( $this->logger){
                $this->logger->info("Api.loadRequiredFields start");
                $this->logger->debug("Api.loadRequiredFields arguments", array( '$request'=> $request ) );
            }

            $fields = $this->connect()->loadRequiredFields($this->auth(), $request);

            if ($fields instanceof ApiAwareInterface) {
                $fields->setApi($this);
            }

            if ($fields instanceof DataTypesFactoryAwareInterface) {
                $fields->setDataTypesFactory($this->typesFactory());
            }

            return $fields;
        }
        catch( \Exception $e ){
            if( $this->logger){
                $this->logger->error("Api.loadRequiredFields exception", array('exception'=>$e));
            }

            throw $e;
        }
        finally {
            $time = \PHP_Timer::stop();
            if( $this->logger){
                $this->logger->info("Api.loadRequiredFields end", array('took'=>\PHP_Timer::secondsToTimeString($time)));
            }
        }
    }
}

