<?php
namespace LineMetrics\LM3\Collection;

interface BaseCollectionInterface extends \Iterator, \ArrayAccess, \Countable
{
    /**
     * Returns first item
     *
     * @return BaseObjectInterface|null
     */
    public function first();

     /**
     * Returns last item
     *
     * @return BaseObjectInterface|null
     */
    public function last();


    /**
     * Returns keys
     *
     * @return array
     */
    public function keys();
}

