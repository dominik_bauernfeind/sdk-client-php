<?php
namespace LineMetrics\LM3\Collection;

interface ContractInterface
{
    public function loadItem(  $value );

    public function isValidItem(  $value );
}