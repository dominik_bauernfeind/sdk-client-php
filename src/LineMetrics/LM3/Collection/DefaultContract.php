<?php
namespace LineMetrics\LM3\Collection;

class DefaultContract implements ContractInterface
{

    public function loadItem($value)
    {
        return $value;
    }

    public function isValidItem($value)
    {
        return true;
    }
}