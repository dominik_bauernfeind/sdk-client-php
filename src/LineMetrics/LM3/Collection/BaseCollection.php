<?php
namespace LineMetrics\LM3\Collection;

class BaseCollection implements BaseCollectionInterface
{

    /**
     * Data
     *
     * @var array
     */
    protected $items = array();

    /**
     * Collection contract
     *
     * @var ContractInterface
     */
    protected $contract = null;


    public function __construct(array $items, ContractInterface $contract = null)
    {
        $this->items = $items;
        $this->contract = $contract ? $contract : new DefaultContract();
    }

    public function valid()
    {
        return isset($this->items[$this->key()]);
    }

    public function next()
    {
        next($this->items);
    }

    public function current()
    {
        return $this->offsetGet($this->key());
    }

    public function rewind()
    {
        return reset($this->items);
    }

    public function key()
    {
        return key($this->items);
    }
    /*
     * (non-PHPdoc)
     * @see ArrayAccess::offsetExists()
     */
    public function offsetExists($offset)
    {
        return array_key_exists($offset, $this->items);
    }

    /*
     * (non-PHPdoc)
     * @see ArrayAccess::offsetGet()
     */
    public function offsetGet($offset)
    {
        if ($this->offsetExists($offset)) {
            $value = $this->items[$offset];

            if (!$this->contract->isValidItem($value)) {
                $this->items[$offset] = $this->contract->loadItem($value);
                $this->initItem( $this->items[$offset] );
            }
            return $this->items[$offset];
        } else {
            return null;
        }
    }

    /**
     *
     * @see ArrayAccess::offsetSet()
     */
    public function offsetSet($offset, $value)
    {
        if ((is_int($offset) || is_string($offset)) && $this->contract->isValidItem($value)) {
            $this->items[$offset] = $value;
        }
    }

    /**
     *
     * @see ArrayAccess::offsetUnset()
     */
    public function offsetUnset($offset)
    {
        if ($this->offsetExists($offset)) {
            unset($this->items[$offset]);
        } else {
            return null;
        }
    }

    /**
     *
     * @see Countable::count()
     */
    public function count()
    {
        return count($this->items);
    }

    /**
     *
     * @see \LineMetrics\LM3\ObjectTypes\ObjectTypesCollectionInterface::first()
     */
    public function first()
    {
        $keys = array_keys($this->items);
        return count($keys) ? $this->offsetGet($keys[0]) : null;
    }

    /**
     *
     * @see \LineMetrics\LM3\ObjectTypes\ObjectTypesCollectionInterface::last()
     */
    public function last()
    {
        $keys = array_keys($this->items);
        return count($keys) ? $this->offsetGet($keys[count($keys) - 1]) : null;
    }


    public function keys()
    {
        return array_keys($this->items);
    }


    protected function initItem( $value){

    }

    public function map( $callback){
        return array_map($callback, $this->items);
    }

    public function toArray(){
        return $this->items;
    }
}
