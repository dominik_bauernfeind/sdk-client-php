<?php
namespace LineMetrics\LM3\Exception;

class BadRequestException extends RequestException implements LM3ExceptionInterface
{
    public function __construct( $url, $response, \Exception $previous = null ){
        parent::__construct( $url, $response, 'Bad request', 400, $previous);
    }
}