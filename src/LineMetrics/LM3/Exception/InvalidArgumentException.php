<?php
namespace LineMetrics\LM3\Exception;

class InvalidArgumentException extends \InvalidArgumentException implements LM3ExceptionInterface
{

}