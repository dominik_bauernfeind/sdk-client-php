<?php
namespace LineMetrics\LM3\Exception;

class RequestException extends \RuntimeException implements LM3ExceptionInterface
{
    protected  $response;

    protected $url;

    public  function __construct( $url, $response, $message, $code, \Exception $previous = null){
        parent::__construct($message, $code, $previous);
        $this->response  = $response;
        $this->url  = $url;
    }

    public function getResponse(){
        return $this->response;
    }

    public function getUrl(){
        return $this->url;
    }
}
