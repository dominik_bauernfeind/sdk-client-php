<?php
namespace LineMetrics\LM3\Exception;

class CurlException extends \RuntimeException implements LM3ExceptionInterface
{
    public  function __construct( $curlMessage, $curlErrorCode, \Exception $previous = null){
        parent::__construct( $curlMessage, $curlErrorCode, $previous);
    }
}
