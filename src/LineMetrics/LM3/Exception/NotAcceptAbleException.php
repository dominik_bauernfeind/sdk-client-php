<?php
namespace LineMetrics\LM3\Exception;

class NotAcceptAbleException extends RequestException
{

    protected $expectedType;

    protected $gotType;

    public function __construct($url, $response, $expectedType, $gotType, \Exception $previous = null)
    {
        parent::__construct($url, $response, sprintf('Content-Type not acceptable. Requested "%s" got "%s"', $expectedType, $gotType), 406, $previous);
        $this->expectedType = $expectedType;
        $this->gotType = $gotType;
    }

    /**
     * Returns the expected content type
     *
     * @return string
     */
    public function getExpectedType()
    {
        return $this->expectedType;
    }

    /**
     * Returns the got content type
     *
     * @return string
     */
    public function getGotType()
    {
        return $this->gotType;
    }
}