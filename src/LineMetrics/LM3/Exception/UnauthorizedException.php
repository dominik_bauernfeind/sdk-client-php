<?php
namespace LineMetrics\LM3\Exception;


class UnauthorizedException extends RequestException
{
    public function __construct( $url, $response, \Exception $previous = null ){
        parent::__construct( $url, $response, 'Unauthorized', 401, $previous);
    }
}