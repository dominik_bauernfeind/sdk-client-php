<?php
namespace LineMetrics\LM3\Exception;

class NotFoundException extends RequestException
{
    public function __construct( $url, $response, \Exception $previous = null ){
        parent::__construct( $url, $response, 'Not found', 404, $previous);
    }
}