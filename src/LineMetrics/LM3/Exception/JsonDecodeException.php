<?php
namespace LineMetrics\LM3\Exception;

class JsonDecodeException extends \RuntimeException implements LM3ExceptionInterface
{
    protected  $jsonMsg;

    protected  $invalidJson;

    public function __construct( $invalidJSON, $jsonErrorMsg, $jsonErrCode, \Exception $previous = null ){
        parent::__construct( sprintf( 'JSON decoding error "%s"', $jsonErrorMsg ) , $jsonErrCode, $previous);
        $this->jsonMsg = $jsonErrorMsg;
        $this->invalidJson = $invalidJSON;
    }

    public function getInvalidJson(){
        return $this->invalidJson;
    }

    public function getJsonErrorMessage(){
        return $this->jsonMsg;
    }

    public function getJsonErrorCode(){
        return $this->getCode();
    }
}
