<?php
namespace LineMetrics\LM3\Exception;

class RequiredOptionException extends ConfigurationException
{
    public function __construct($optionRequired, $code = null, \Exception $previous = null){
        parent::__construct( sprintf('Option "%s" is required', $optionRequired), $code, $previous) ;
    }
}
