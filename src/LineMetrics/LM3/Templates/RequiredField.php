<?php
namespace LineMetrics\LM3\Templates;

use \LineMetrics\LM3\AliasAwareTrait;
use \LineMetrics\LM3\UidAwareTrait;

class RequiredField implements RequiredFieldInterface
{
    use AliasAwareTrait;
    use UidAwareTrait;

    protected $dataType;

    /**
     *
     * @see \LineMetrics\LM3\Templates\RequiredFieldInterface::setDataType()
     */
    public function setDataType($dataType)
    {
        $this->dataType = $dataType;
        return $this;
    }

    /**
     *
     * @see \LineMetrics\LM3\Templates\RequiredFieldInterface::getDataType()
     */
    public function getDataType()
    {
        return $this->dataType;
    }
}
