<?php
namespace LineMetrics\LM3\Templates;

use LineMetrics\LM3\Collection\ContractInterface;
use LineMetrics\LM3\Templates\Template;
use LineMetrics\LM3\Templates\TemplateInterface;

class TemplatesContract implements ContractInterface
{
    public function loadItem($value)
    {
        $item = new Template();
        $item->setUid($value->uid);
        $item->setName($value->name);
        return $item;
    }

    public function isValidItem($value)
    {
        return $value instanceof TemplateInterface;
    }
}