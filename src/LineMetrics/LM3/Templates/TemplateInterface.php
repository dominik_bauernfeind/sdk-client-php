<?php
namespace LineMetrics\LM3\Templates;

use LineMetrics\LM3\Api\ApiAwareInterface;
use LineMetrics\LM3\UidAwareInterface;

interface TemplateInterface extends ApiAwareInterface, UidAwareInterface
{
    /**
     * Sets name
     *
     * @param string $name
     * @return TemplateInterface
     */
    public function setName( $name );

    /**
     * Gets name
     *
     * @return string
     */
    public function getName();

    /**
     * Loads required fields
     *
     * @return RequiredFieldsCollectionInterface
     */
    public function loadRequiredFields();

    /**
     *
     */
    public function createAssets();
}