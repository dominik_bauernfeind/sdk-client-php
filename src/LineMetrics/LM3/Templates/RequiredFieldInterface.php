<?php
namespace LineMetrics\LM3\Templates;
use \LineMetrics\LM3\AliasAwareInterface;
use \LineMetrics\LM3\UidAwareInterface;

interface RequiredFieldInterface extends AliasAwareInterface, UidAwareInterface
{
    /**
     * Sets data type
     *
     * @param string|null $dataType
     */
    public function setDataType( $dataType );

    /**
     * Gets data type
     *
     * @return string|null
     */
    public function getDataType();
}