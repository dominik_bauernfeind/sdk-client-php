<?php
namespace LineMetrics\LM3\Templates;

use LineMetrics\LM3\Collection\ContractInterface;

class RequiredFieldsContract implements ContractInterface
{
    public function loadItem($value)
    {
        $item = new RequiredField();
        $item->setUid($value->uid);
        $item->setDataType($value->data_type);
        $item->setAlias($value->alias);
        return $item;
    }

    public function isValidItem($value)
    {
        return $value instanceof RequiredFieldInterface;
    }
}

