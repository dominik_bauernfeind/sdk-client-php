<?php
namespace LineMetrics\LM3\Templates;

use LineMetrics\LM3\Collection\BaseCollectionInterface;

interface RequiredFieldsCollectionInterface extends BaseCollectionInterface
{
    /**
     * Finds required field by alias
     *
     * @param string $alias
     * @return RequiredFieldInterface|false
     */
    public function findOneByAlias($alias);

    /**
     * Finds required field by uid
     *
     * @param string $uid
     * @return RequiredFieldInterface|false
     */
    public function findOneByUid( $uid );
}