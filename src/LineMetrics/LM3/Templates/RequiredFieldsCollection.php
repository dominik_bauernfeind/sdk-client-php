<?php
namespace LineMetrics\LM3\Templates;

use LineMetrics\LM3\Collection\BaseCollection;

class RequiredFieldsCollection extends BaseCollection implements RequiredFieldsCollectionInterface
{

    public function __construct(array $items)
    {
        parent::__construct($items, new RequiredFieldsContract());
    }

    /**
     * (non-PHPdoc)
     *
     * @see \LineMetrics\LM3\Templates\RequiredFieldsCollectionInterface::findOneByAlias()
     */
    public function findOneByAlias($alias)
    {
        $r = array_filter($this->items, function ($x) use($alias)
        {
            if ($x instanceof RequiredFieldInterface) {
                return $x->getAlias() == $alias;
            } else
                if ($x instanceof \stdClass) {
                    return $x->alias == $alias;
                }

            return false;
        });

        if (count($r)) {
            $index = key($r);
            return $this->offsetGet($index);
        } else {
            return false;
        }
    }

    /**
     * (non-PHPdoc)
     * @see \LineMetrics\LM3\Templates\RequiredFieldsCollectionInterface::findOneByUid()
     */
    public function findOneByUid($uid)
    {
        $r = array_filter($this->items, function ($x) use($uid)
        {
            if ($x instanceof TemplateInterface) {
                return $x->getUid() == $uid;
            } else
                if ($x instanceof \stdClass) {
                    return $x->uid == $uid;
                }

            return false;
        });

        if (count($r)) {
            $index = key($r);
            return $this->offsetGet($index);
        } else {
            return false;
        }
    }
}