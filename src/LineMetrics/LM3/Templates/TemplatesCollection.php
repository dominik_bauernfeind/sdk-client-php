<?php
namespace LineMetrics\LM3\Templates;

use LineMetrics\LM3\Collection\BaseCollection;
use LineMetrics\LM3\Api\ApiAwareInterface;

class TemplatesCollection extends BaseCollection implements TemplatesCollectionInterface
{
    protected $api;

    public function __construct(array $items)
    {
        parent::__construct($items, new TemplatesContract());
    }

    public function __destruct(){
        $this->api = null;
    }

    /**
     *
     * @see \LineMetrics\LM3\Api\ApiAwareInterface::getApi()
     */
    public function getApi()
    {
        return $this->api;
    }

    /**
     *
     * @see \LineMetrics\LM3\Api\ApiAwareInterface::setApi()
     * @return TemplateCollection
     */
    public function setApi(\LineMetrics\LM3\Api\ApiInterface $api = null)
    {
        $this->api = $api;
        return $this;
    }

    /**
     * (non-PHPdoc)
     * @see \LineMetrics\LM3\Collection\BaseCollection::initItem()
     */
    protected function initItem( $value){
        if ($value instanceof ApiAwareInterface) {
            $value->setApi($this->api);
        }
    }

    public function findOneByUid( $uid ){
        $r = array_filter($this->items, function($x) use ( $uid ){
            if( $x instanceof TemplateInterface ){
                return $x->getUid() == $x;
            }
            else if( $x instanceof \stdClass ){
                return $x->uid == $uid;
            }

            return false;
        });

        if( count($r)){
            $index =  key($r);
            return $this->offsetGet($index);
        }
        else{
            return false;
        }
    }
}
