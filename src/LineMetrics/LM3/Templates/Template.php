<?php
namespace LineMetrics\LM3\Templates;

use LineMetrics\LM3\RequestTypes\RequiredFieldsRequest;
use LineMetrics\LM3\UidAwareTrait;
use LineMetrics\LM3\Api\ApiAwareTrait;

class Template implements TemplateInterface
{

    use UidAwareTrait;
    use ApiAwareTrait;

    protected $name;

    protected $uid;

    protected $api;

    /*
     * (non-PHPdoc)
     * @see \LineMetrics\LM3\TemplateInterface::setName()
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /*
     * (non-PHPdoc)
     * @see \LineMetrics\LM3\TemplateInterface::getName()
     */
    public function getName()
    {
        return $this->name;
    }



    /*
     * (non-PHPdoc)
     * @see \LineMetrics\LM3\TemplateInterface::createAssets()
     */
    public function createAssets()
    {
        // TODO Auto-generated method stub
    }

    /*
     * (non-PHPdoc)
     * @see \LineMetrics\LM3\TemplateInterface::loadRequiredFields()
     */
    public function loadRequiredFields()
    {
        if (! $this->api) {
            throw new \UnexpectedValueException('No Api interface set');
        }

        $request = new RequiredFieldsRequest();
        $request->setTemplateUid($this->getUid());
        return $this->api->loadRequiredFields($request);
    }
}