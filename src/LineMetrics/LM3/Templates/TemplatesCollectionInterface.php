<?php
namespace LineMetrics\LM3\Templates;

use LineMetrics\LM3\Collection\BaseCollectionInterface;
use LineMetrics\LM3\Api\ApiAwareInterface;

interface TemplatesCollectionInterface extends BaseCollectionInterface, ApiAwareInterface
{


}