<?php
namespace LineMetrics\LM3;

interface AliasAwareInterface
{
    /**
     * Sets alias
     *
     * @param string|null $alias
     * @return AliasAwareInterface
     */
    public function setAlias($alias);

    /**
     * Gets alias
     *
     * @return string|null
     */
    public function getAlias();
}