<?php
namespace LineMetrics\LM3\Curl\Auth;

use LineMetrics\LM3\Auth\AuthenticatorInterface;
use LineMetrics\LM3\Auth\AuthToken;
use LineMetrics\LM3\Exception\CurlException;
use LineMetrics\LM3\Exception\BadRequestException;
use LineMetrics\LM3\Exception\JsonDecodeException;
use LineMetrics\LM3\Exception\NotAcceptAbleException;
use LineMetrics\LM3\Exception\RequestException;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerAwareTrait;
use LineMetrics\LM3\Curl\CurlFactoryInterface;
use LineMetrics\LM3\Curl\CurlFactory;
use Psr\Log\LoggerInterface;

class ClientCredentialsGrant implements AuthenticatorInterface, LoggerAwareInterface
{

    const ENDPOINT_URL = 'https://lm3api.linemetrics.com/oauth/access_token';

    /**
     * Client id
     *
     * @var string
     */
    protected $clientId;

    /**
     * Client secret
     *
     * @var string
     */
    protected $clientSecret;

    /**
     * Curl factory
     *
     * @var CurlFactoryInterface
     */
    protected $curlFactory;

    /**
     * Token endpoint
     *
     * @var string
     */
    protected $endPoint;

    use LoggerAwareTrait;

    public function __construct($clientId, $clientSecret, $endPoint = nulL, CurlFactoryInterface $factory = null, LoggerInterface $logger = null)
    {
        $this->clientId = $clientId;
        $this->clientSecret = $clientSecret;

        if (! $factory) {
            $this->curlFactory = new CurlFactory();
        } else {
            $this->curlFactory = $factory;
        }

        if(!$endPoint){
            $endPoint = self::ENDPOINT_URL;
        }

        $this->endPoint = $endPoint;
        $this->logger = $logger;
    }

    /**
     *
     * @see \LineMetrics\LM3\Auth\AuthenticatorInterface::auth()
     * @throws CurlException Error in curl_exec
     * @throws NotAcceptAbleException Server did not return Content-Type: application/json
     * @throws JsonDecodeException Json could not be decoded
     * @throws BadRequestException Server returned HTTP 400
     * @throws RequestException Any other server error
     * @return AuthTokenInterface The access token
     */
    public function auth()
    {
        \PHP_Timer::start();

        try{
            if( $this->logger){
                $this->logger->info("AuthClientCredentials.auth start");
            }

            $postData = array(
                'client_id' => $this->clientId,
                'client_secret' => $this->clientSecret,
                'grant_type' => 'client_credentials'
            );


            if( $this->logger){
                $this->logger->debug("AuthClientCredentials.auth grant", array( 'post'=> $postData ) );
            }


            $ch = $this->curlFactory->createCurl();

            curl_setopt($ch, \CURLOPT_URL, $this->endPoint);
            curl_setopt($ch, \CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($ch, \CURLOPT_POST, TRUE);
            curl_setopt($ch, \CURLOPT_POSTFIELDS, $postData);
            $stringResponse = curl_exec($ch);

            $httpStatus = curl_getinfo($ch, \CURLINFO_HTTP_CODE);
            $contentType = curl_getinfo($ch, \CURLINFO_CONTENT_TYPE);

            if( $this->logger){
                $this->logger->debug("AuthClientCredentials.auth response", array('response' => $stringResponse, 'status'=>$httpStatus, 'contentType'=>$contentType ) );
            }

            if (FALSE === $stringResponse) {
                $e = new CurlException(curl_error($ch), curl_errno($ch));
                curl_close($ch);
                throw $e;
            }


            curl_close($ch);

            switch ($httpStatus) {
                case 200:
                    {
                        if ($contentType != "application/json") {
                            throw new NotAcceptAbleException($this->endPoint, $stringResponse, "application/json", $contentType);
                        }

                        $jsonResponse = json_decode($stringResponse);

                        if (FALSE === $jsonResponse) {
                            throw new JsonDecodeException($stringResponse, json_last_error_msg(), json_last_error());
                        }

                        return new AuthToken($jsonResponse->access_token, $jsonResponse->token_type, $jsonResponse->expires_in);
                    }
                    break;

                case 400:
                    throw new BadRequestException($this->endPoint, $stringResponse);
                    break;

                default:
                    throw new RequestException($this->endPoint, $stringResponse, sprintf('Request error. Server responded with status "%d"', $httpStatus), $httpStatus);
                    break;
            }
        }
        catch( \Exception $e ){
            if( $this->logger){
                $this->logger->error("AuthClientCredentials.auth exception", array('exception'=>$e));
            }

            throw $e;
        }
        finally {
            $time = \PHP_Timer::stop();
            if( $this->logger){
                $this->logger->info("AuthClientCredentials.auth end", array('took'=>\PHP_Timer::secondsToTimeString($time)));
            }
        }
    }
}