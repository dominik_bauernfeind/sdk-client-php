<?php
namespace LineMetrics\LM3\Curl;

use LineMetrics\LM3\Exception\RuntimeException;
class CurlFactory implements CurlFactoryInterface
{
    public static $defaults = array(
        \CURLOPT_SSL_VERIFYHOST => 2,
        \CURLOPT_SSL_VERIFYPEER => 1,
        // \CURLOPT_USERAGENT,
        \CURLOPT_FOLLOWLOCATION => TRUE
    );


    protected $options;

    public function __construct( array $options = array() ){

        $this->options = static::$defaults;

        foreach( $options as $k => $v){
            $this->options[$k] = $v;
        }
    }

	/* (non-PHPdoc)
     * @see \LineMetrics\LM3\Curl\CurlFactoryInterface::createCurl()
     */
    public function createCurl()
    {
        if (! extension_loaded('curl')) {
            new RuntimeException('Curl extension required');
        }

        $curl = curl_init();

        foreach ($this->options as $key => $value) {
            curl_setopt($curl, $key, $value);
        }

        return $curl;
    }
}

CurlFactory::$defaults[\CURLOPT_CAINFO] = (__DIR__ . '/../../../../resources/cacert/cacert.pem');
