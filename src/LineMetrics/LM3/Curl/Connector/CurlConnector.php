<?php
namespace LineMetrics\LM3\Curl\Connector;

use LineMetrics\LM3\Connector\ConnectorInterface;
use LineMetrics\LM3\Auth\AuthTokenInterface;
use LineMetrics\LM3\Connector\ConnectorException;
use LineMetrics\LM3\RequestTypes\AssetRequestInterface;
use LineMetrics\LM3\RequestTypes\UpdateObjectRequestInterface;
use LineMetrics\LM3\RequestTypes\ObjectRequestInterface;
use LineMetrics\LM3\RequestTypes\ConfigRequestInterface;
use LineMetrics\LM3\RequestTypes\LoadDataRequestInterface;
use LineMetrics\LM3\RequestTypes\LoadLastValueRequestInterface;
use LineMetrics\LM3\ObjectTypes\ObjectTypesSerializerInterface;
use LineMetrics\LM3\ObjectTypes\ObjectTypesSerializer;
use LineMetrics\LM3\ObjectTypes\ObjectTypesCollectionInterface;
use LineMetrics\LM3\DataTypes\Configuration;
use LineMetrics\LM3\DataTypes\DataTypesCollection;
use LineMetrics\LM3\DataTypes\ConfigurationInterface;
use LineMetrics\LM3\DataTypes\Collection\DoubleAverageCollection;
use LineMetrics\LM3\DataTypes\Collection\DoubleCollection;
use LineMetrics\LM3\DataTypes\Collection\BooleanCollection;
use LineMetrics\LM3\DataTypes\Collection\StringCollection;
use LineMetrics\LM3\DataTypes\Collection\TableCollection;
use LineMetrics\LM3\DataTypes\Collection\GeoCoordCollection;
use LineMetrics\LM3\DataTypes\Serializer\DoubleSerializer;
use LineMetrics\LM3\DataTypes\Serializer\DoubleAverageSerializer;
use LineMetrics\LM3\DataTypes\Serializer\BooleanSerializer;
use LineMetrics\LM3\DataTypes\Serializer\GeoCoordSerializer;
use LineMetrics\LM3\DataTypes\Serializer\StringSerializer;
use LineMetrics\LM3\DataTypes\Serializer\TableSerializer;
use LineMetrics\LM3\DataTypes\DataTypesSerializerInterface;
use LineMetrics\LM3\DataTypes\DataTypesCollectionInterface;
use LineMetrics\LM3\DataTypes\BaseTypeInterface;
use LineMetrics\LM3\ObjectTypes\ObjectTypesCollection;
use LineMetrics\LM3\Templates\TemplatesCollection;
use LineMetrics\LM3\Templates\TemplatesCollectionInterface;
use LineMetrics\LM3\Templates\RequiredFieldsCollection;
use LineMetrics\LM3\Templates\RequiredFieldsCollectionInterface;
use LineMetrics\LM3\Exception\CurlException;
use LineMetrics\LM3\Exception\UnauthorizedException;
use LineMetrics\LM3\Exception\BadRequestException;
use LineMetrics\LM3\Exception\NotFoundException;
use LineMetrics\LM3\Exception\JsonDecodeException;
use LineMetrics\LM3\Exception\RequestException;
use LineMetrics\LM3\Exception\NotAcceptAbleException;
use LineMetrics\LM3\Exception\InvalidArgumentException;
use LineMetrics\LM3\Exception\UnexpectedValueException;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerAwareTrait;
use LineMetrics\LM3\Curl\CurlFactoryInterface;
use LineMetrics\LM3\Curl\CurlFactory;
use Psr\Log\LoggerInterface;

class CurlConnector implements ConnectorInterface, LoggerAwareInterface
{

    const ENDPOINT_URL = 'https://lm3api.linemetrics.com/v2';

    /**
     * Curl factory
     *
     * @var CurlFactoryInterface
     */
    protected $curlFactory;

    /**
     * Api endpoint
     *
     * @var string
     */
    protected $endPoint;


    /**
     * Curl resource
     *
     * @var resource
     */
    protected $curl;

    use LoggerAwareTrait;

    public function __construct( $endPoint = null, CurlFactoryInterface $factory = null, LoggerInterface $logger = null)
    {
        if(!$factory){
            $this->curlFactory = new CurlFactory();
        }
        else{
            $this->curlFactory = $factory;
        }

        if(!$endPoint){
            $endPoint = self::ENDPOINT_URL;
        }

        $this->endPoint = $endPoint;
        $this->curl = $this->curlFactory->createCurl();
        $this->logger = $logger;
    }


    public function __destruct(){
        if( $this->curl){
            curl_close($this->curl);
            $this->curl = null;
        }
    }

    /**
     *
     * @see \LineMetrics\LM3\Connector\ConnectorInterface::load()
     * @throws UnexpectedValueException Property <b>ObjectId</b> is not set on $request
     */
    public function load(AuthTokenInterface $accessToken, ObjectRequestInterface $request)
    {
        \PHP_Timer::start();

        try{
            if( $this->logger){
                $this->logger->info("ConnectorCurl.load start");
                $this->logger->debug("ConnectorCurl.load arguments", array( '$request'=> $request ) );
            }

            $objectId = $request->getObjectId();

            if (! strlen($objectId)) {
                throw new UnexpectedValueException('Property "ObjectId" is required');
            }

            $url = $this->endPoint  . '/object/' . $objectId;
            $jsonResponse = $this->callApi($accessToken, $url, "GET");

            return $this->createObjectTypesSerializer()->unserialize($jsonResponse);
        }catch( \Exception $e ){
            if( $this->logger){
                $this->logger->error("ConnectorCurl.load exception", array('exception'=>$e));
            }

            throw $e;
        }
        finally {
            $time = \PHP_Timer::stop();
            if( $this->logger){
                $this->logger->info("ConnectorCurl.load end", array('took'=>\PHP_Timer::secondsToTimeString($time)));
            }
        }
    }

    /**
     *
     * @see \LineMetrics\LM3\Connector\ConnectorInterface::loadAssets()
     * @throws UnexpectedValueException If $request != null when Property <b>ObjectId</b> is not set on $request
     */
    public function loadAssets(AuthTokenInterface $accessToken, AssetRequestInterface $request = null)
    {
        $idSegment = array();
        $query = '';
        $objectId = '';

        if ($request) {
            $objectId = $request->getObjectId();

            if (! strlen($objectId)) {
                throw new UnexpectedValueException('Property "ObjectId" is required');
            }
            $query = http_build_query($request->getQueryParams());
        }

        $url = $this->endPoint  . '/children/' . $objectId . (strlen($query) ? '?' . $query : '');
        $jsonResponse = $this->callApi($accessToken, $url);

        $objects = array_filter($jsonResponse,
                function ($x) use($request)
                {
                    if ($request && $request->getObjectType()) {
                        return $x->object_type == $request->getObjectType();
                    } else {
                        return $x->object_type == "object";
                    }
                });

        return $this->createObjectTypesCollection(array_merge($objects));
    }

    /**
     *
     * @see \LineMetrics\LM3\Connector\ConnectorInterface::updateObject()
     * @throws UnexpectedValueException Property <b>ObjectId</b> is not set on $request
     */
    public function updateObject(AuthTokenInterface $accessToken, UpdateObjectRequestInterface $request)
    {
        $objectId = $request->getObjectId();

        if (! strlen($objectId)) {
            throw new UnexpectedValueException('Property "ObjectId" is required');
        }

        $url = $this->endPoint  . '/object/' . $objectId;
        $jsonResponse = $this->callApi($accessToken, $url, "POST", json_encode($request->getQueryParams()));
        return $jsonResponse->message;
    }

    /**
     *
     * @see \LineMetrics\LM3\Connector\ConnectorInterface::loadConfig()
     * @throws UnexpectedValueException Property <b>ObjectId</b> is not set on $request
     */
    public function loadConfig(\LineMetrics\LM3\Auth\AuthTokenInterface $accessToken, ConfigRequestInterface $request)
    {
        $objectId = $request->getObjectId();

        if (! strlen($objectId)) {
            throw new UnexpectedValueException('Property "ObjectId" is required');
        }

        $url = $this->endPoint  . '/data/' . $objectId . '/config';
        $jsonResponse = $this->callApi($accessToken, $url);
        return new Configuration($jsonResponse->input, $jsonResponse->output);
    }

    /**
     *
     * @see \LineMetrics\LM3\Connector\ConnectorInterface::loadData()
     * @throws UnexpectedValueException In terms of
     *         <ul>
     *         <li>Property <b>ObjectId</b> is not set on $request</li>
     *         <li>Property <b>From</b> is not set on $request</li>
     *         <li>Property <b>To</b> is not set on $request</li>
     *         </ul>
     */
    public function loadData(\LineMetrics\LM3\Auth\AuthTokenInterface $accessToken, LoadDataRequestInterface $request, ConfigurationInterface $output = null)
    {
        $objectId = $request->getObjectId();

        if (! strlen($objectId)) {
            throw new UnexpectedValueException('Property "ObjectId" is required');
        }

        $from = $request->getFrom();

        if (empty($from)) {
            throw new UnexpectedValueException('Property "From" is required');
        }

        $to = $request->getTo();

        if (empty($to)) {
            throw new UnexpectedValueException('Property "To" is required');
        }

        $query = http_build_query($request->getQueryParams());
        $url = $this->endPoint  . '/data/' . $objectId . '?' . $query;
        $jsonResponse = $this->callApi($accessToken, $url);
        return $this->createDataTypesCollection($jsonResponse, $output);
    }

    /**
     *
     * @see \LineMetrics\LM3\Connector\ConnectorInterface::loadLastValue()
     * @throws UnexpectedValueException Property <b>ObjectId</b> is not set on $request
     */
    public function loadLastValue(\LineMetrics\LM3\Auth\AuthTokenInterface $accessToken, LoadLastValueRequestInterface $request, ConfigurationInterface $output = null)
    {
        $objectId = $request->getObjectId();

        if (! strlen($objectId)) {
            throw new UnexpectedValueException('Property "ObjectId" is required');
        }

        $query = http_build_query($request->getQueryParams());
        $url = $this->endPoint  . '/data/' . $objectId . '?' . $query;

        $jsonResponse = $this->callApi($accessToken, $url);

        if ($output) {
            if( count( $jsonResponse) ){
                return $this->createDataTypesSerializer($output)->unserialize($jsonResponse[0]);
            }
            else{
                return null;
            }
        } else {
            return $jsonResponse;
        }
    }

    /**
     *
     * @see \LineMetrics\LM3\Connector\ConnectorInterface::writeData()
     * @throws UnexpectedValueException In terms of
     *         <ul>
     *         <li>Property <b>ObjectId</b> is not set on $request</li>
     *         <li>Property <b>Data</b> is not set on $request</li>
     *         </ul>
     */
    public function writeData(\LineMetrics\LM3\Auth\AuthTokenInterface $accessToken, \LineMetrics\LM3\RequestTypes\WriteDataRequestInterface $request)
    {
        
        $objectId = $request->getObjectId();

        if (! strlen($objectId)) {
            throw new UnexpectedValueException('Property "ObjectId" is required');
        }

        $data = $request->getData();

        if (empty($data)) {
            throw new UnexpectedValueException('Property "Data" is required');
        }

        $dataList = array();


        


        foreach ($data as $index => $value) {

            if ($value instanceof BaseTypeInterface === FALSE) {
                throw new ConnectorException(
                        'Invalid value found on index "' . $index . '" Got "' . (is_object($value) ? get_class($value) : gettype($value)) . '" expected "instanceof BaseTypeInterface"');
            }

            
            $dataList[$index] = $value->jsonSerialize();
        }
          

        $url = $this->endPoint  . '/data/' . $objectId;
        
        $jsonResponse = $this->callApi($accessToken, $url, "POST", json_encode($dataList, $request->getEncodingFlags() ));

        $return = new \ArrayObject(array());

        foreach( $jsonResponse as $key => $response){
           $return->offsetSet($key, $response->response);
        }

        return $return;
    }

    /**
     *
     * @see \LineMetrics\LM3\Connector\ConnectorInterface::loadTemplates()
     */
    public function loadTemplates(\LineMetrics\LM3\Auth\AuthTokenInterface $accessToken, \LineMetrics\LM3\RequestTypes\TemplatesRequestInterface $request = null)
    {
        if ($request) {
            $query = http_build_query($request->getQueryParams());
        } else {
            $query = '';
        }

        $url = $this->endPoint  . '/templates' . (strlen($query) ? '?' . $query : '');
        $jsonResponse = $this->callApi($accessToken, $url);
        return $this->createTemplatesCollection($jsonResponse);
    }

    /**
     *
     * @see \LineMetrics\LM3\Connector\ConnectorInterface::loadRequiredFields()
     * @throws UnexpectedValueException In terms of
     *         <ul>
     *         <li>Property <b>Uid</b> is not set on $request</li>
     *         </ul>
     */
    public function loadRequiredFields(\LineMetrics\LM3\Auth\AuthTokenInterface $accessToken, \LineMetrics\LM3\RequestTypes\RequiredFieldsRequestInterface $request)
    {
        $uid = $request->getTemplateUid();

        if (! strlen($uid)) {
            throw new UnexpectedValueException('Property "Uid" is required');
        }

        $url = $this->endPoint  . '/template/' . urlencode($uid) . '/required-fields';
        $jsonResponse = $this->callApi($accessToken, $url);
        return $this->createRequiredFieldsCollection($jsonResponse);
    }

    /**
     * Makes the HTTP request to teh LM api
     *
     * @param AuthTokenInterface $accessToken
     *            The acces tokne
     * @param stirng $url
     *            The REST url
     * @param string $method
     *            ( OPTIONAL) The http method
     * @param string $jsonPayload
     *            ( OPTIONAL) The $POST json paylod
     * @throws InvalidArgumentException Unsupported http method provied
     * @throws CurlException Error in curl_exec
     * @throws NotAcceptAbleException Server did not return Content-Type: application/json
     * @throws JsonDecodeException Json could not be decoded
     * @throws UnauthorizedException Server returned HTTP 401
     * @throws BadRequestException Server returned HTTP 400
     * @throws NotFoundException Server returned HTTP 404
     * @throws RequestException Any other server error
     * @return \stdClas
     */
    protected function callApi(AuthTokenInterface $accessToken, $url, $method = "GET", $jsonPayload = "")
    {
        \PHP_Timer::start();

        try{
            if( $this->logger){
                $this->logger->info("ConnectorCurl.callApi start", array('method'=>$method, 'url'=>$url));
                $this->logger->debug("ConnectorCurl.callApi arguments", array( '$url'=> $url, '$method'=>$method, '$accessToken'=>$accessToken, '$jsonPayload'=>$jsonPayload ) );
            }

            curl_setopt($this->curl, \CURLOPT_URL, $url);
            curl_setopt($this->curl, \CURLOPT_RETURNTRANSFER, TRUE);
            // curl_setopt($this->curl, \CURLOPT_VERBOSE, TRUE);
            // $curl_buffer = fopen('php://temp', 'w+');
            // curl_setopt( $this->curl, \CURLOPT_STDERR, $curl_buffer);

            //echo $method . " " . $url . "\n";


            $header = array(
                'Authorization: Bearer ' . $accessToken->getAccesToken(),
                'Accept: application/json'
            );

            switch (strtolower($method)) {
                case "get":
                    curl_setopt($this->curl, \CURLOPT_POST, FALSE);
                    break;

                case "post":
                    curl_setopt($this->curl, \CURLOPT_POST, TRUE);
                    curl_setopt($this->curl, \CURLOPT_POSTFIELDS, $jsonPayload);
                    $header[] = "Content-Type: application/json";
                    break;

                case "delete":
                    curl_setopt($this->curl, CURLOPT_CUSTOMREQUEST, "DELETE");
                    break;

                default:
                    throw new InvalidArgumentException('Unsupported HTTP method "' . $method . '"');
                    break;
            }

            if( $this->logger){
                $this->logger->debug("ConnectorCurl.callApi headers", $header );
            }

            curl_setopt($this->curl, \CURLOPT_HTTPHEADER, $header);

            $strinResponse = curl_exec($this->curl);
            $httpStatus = curl_getinfo($this->curl, \CURLINFO_HTTP_CODE);
            $contentType = curl_getinfo($this->curl, \CURLINFO_CONTENT_TYPE);


            if( $this->logger){
                $this->logger->debug("ConnectorCurl.callApi API response", array('response' => $strinResponse, 'status'=>$httpStatus, 'contentType'=>$contentType ) );
            }


            if (FALSE === $strinResponse) {
                throw new CurlException(curl_error($this->curl), curl_errno($this->curl));
            }

            switch ($httpStatus) {
                case 200:
                    {
                        if ($contentType != "application/json") {
                            throw new NotAcceptAbleException($url, $strinResponse, "application/json", $contentType);
                        }

                        $jsonResponse = json_decode($strinResponse);
                        


                        if (FALSE === $jsonResponse) {
                            throw new JsonDecodeException($strinResponse, json_last_error_msg(), json_last_error());
                        }

                        
                        return $jsonResponse;
                    }
                    break;

                case 401:
                    throw new UnauthorizedException($url, $strinResponse);
                    break;

                case 400:
                    throw new BadRequestException($url, $strinResponse);
                    break;

                case 404:
                    throw new NotFoundException($url, $strinResponse);
                    break;

                default:
                    throw new RequestException($url, $strinResponse, sprintf('Request error. Server responded with status "%d"', $httpStatus), $httpStatus);
                    break;
            }
        }
        catch( \Exception $e ){
            if( $this->logger){
                $this->logger->error("ConnectorCurl.callApi exception", array('exception'=>$e));
            }

            throw $e;
        }
        finally {
            $time = \PHP_Timer::stop();
            if( $this->logger){
                $this->logger->info("ConnectorCurl.callApi end", array('took'=>\PHP_Timer::secondsToTimeString($time)));
            }
        }
    }

    /**
     * Creates a object types collection
     *
     * @param array $items
     * @return ObjectTypesCollectionInterface
     */
    protected function createObjectTypesCollection(array $items)
    {
        return new ObjectTypesCollection($items);
    }

    /**
     * Creates a templates collection
     *
     * @param array $items
     * @return TemplatesCollectionInterface
     */
    protected function createTemplatesCollection(array $items)
    {
        return new TemplatesCollection($items);
    }

    /**
     * Creates a required fields collection
     *
     * @param array $items
     * @return RequiredFieldsCollectionInterface
     */
    protected function createRequiredFieldsCollection(array $items)
    {
        return new RequiredFieldsCollection($items);
    }

    /**
     * Creates a data types collection
     *
     * @param array $items
     * @param ConfigurationInterface $output
     * @return DataTypesCollectionInterface
     * @throws UnexpectedValueException Unknown data type in $output->getOutput()
     */
    protected function createDataTypesCollection(array $items, ConfigurationInterface $output = null)
    {
        if (! $output) {
            return new DataTypesCollection($items);
        } else {
            switch (strtolower($output->getOutput())) {
                case "doubleaverage":
                    return new DoubleAverageCollection($items);
                    break;

                case "double":
                    return new DoubleCollection($items);
                    break;

                case "boolean":
                    return new BooleanCollection($items);
                    break;

                case "geocoord":
                    return new GeoCoordCollection($items);
                    break;

                case "string":
                    return new StringCollection($items);
                    break;
                case "table":
                    return new TableCollection($items);
                    break;
                default:
                    throw new UnexpectedValueException('Unknown data type "' . $output->getOutput() . '"');
                    break;
            }
        }
    }

    /**
     * Creates a data types serializer
     *
     * @param ConfigurationInterface $output
     * @return DataTypesSerializerInterface
     * @throws UnexpectedValueException Unknown data type in $output->getOutput()
     */
    protected function createDataTypesSerializer(ConfigurationInterface $output)
    {
        switch (strtolower($output->getOutput())) {
            case "doubleaverage":
                return new DoubleAverageSerializer();
                break;

            case "double":
                return new DoubleSerializer();
                break;

            case "boolean":
                return new BooleanSerializer();
                break;

            case "geocoord":
                return new GeoCoordSerializer();
                break;

            case "string":
                return new StringSerializer();
                break;
            case "table":
                return new TableSerializer();
                break;
            default:
                throw new UnexpectedValueException('Unknown data type "' . $output->getOutput() . '"');
                break;
        }
    }

    /**
     * Creates the object serializer instance
     *
     * @return ObjectTypesSerializerInterface
     */
    protected function createObjectTypesSerializer()
    {
        return new ObjectTypesSerializer();
    }
}
