<?php
namespace LineMetrics\LM3\Curl;

interface CurlFactoryInterface
{
    /**
     * Creates new curl
     *
     * @param array $curlOpts
     * @return resource
     */
    public function createCurl();
}