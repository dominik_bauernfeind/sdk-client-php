<?php
namespace LineMetrics\LM3\Api;


use LineMetrics\LM3\Exception\LM3ExceptionInterface;

class ApiException extends \Exception implements LM3ExceptionInterface
{

}
