<?php
namespace LineMetrics\LM3\Api;

interface ApiAwareInterface
{
    /**
     * Sets api interface
     *
     * @param ApiInterface $api
     * @return ApiAwareInterface
     */
    public function setApi( ApiInterface $api);

}