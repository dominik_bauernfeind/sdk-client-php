<?php
namespace LineMetrics\LM3\Api;
use LineMetrics\LM3\RequestTypes\AssetRequestInterface;
use LineMetrics\LM3\RequestTypes\UpdateObjectRequestInterface;
use LineMetrics\LM3\RequestTypes\ObjectRequestInterface;
use LineMetrics\LM3\RequestTypes\ConfigRequestInterface;
use LineMetrics\LM3\RequestTypes\LoadDataRequestInterface;
use LineMetrics\LM3\RequestTypes\LoadLastValueRequestInterface;
use LineMetrics\LM3\RequestTypes\WriteDataRequestInterface;
use LineMetrics\LM3\RequestTypes\TemplatesRequestInterface;
use LineMetrics\LM3\RequestTypes\RequiredFieldsRequestInterface;
use LineMetrics\LM3\DataTypes\ConfigurationInterface;
use LineMetrics\LM3\DataTypes\DataCollectionInterface;
use LineMetrics\LM3\ObjectTypes\ObjectTypesCollectionInterface;
use LineMetrics\LM3\Templates\TemplatesCollectionInterface;
use LineMetrics\LM3\Templates\RequiredFieldsCollectionInterface;


interface ApiInterface
{

    /**
     * Loads an asset
     *
     * @param ObjectRequestInterface $request
     * @parmam BaseObjectInterface
     */
    public function load( ObjectRequestInterface $request );

    /**
     * Loads child assets
     *
     * To load child assets from first level pass NULL to $request.
     * Method is not recursive.
     *
     * @param AssetRequestInterface $request ( OPTIONAL )
     *
     * @return ObjectTypesCollectionInterface
     */
    public function loadAssets( AssetRequestInterface $request = null );


    /**
     * Updates an object
     *
     * @param UpdateObjectRequestInterface $request The request
     * @return string
     */
    public function updateObject( UpdateObjectRequestInterface $request);


    /**
     * Loads input and output data type configuration
     *
     * @param ConfigRequestInterface $request The request
     * @return ConfigurationInterface
     */
    public function loadConfig( ConfigRequestInterface $request);


    /**
     * Loads data
     *
     * If $output is set the api should try to return a typed collection
     * based on the ouput configuration. If it not possible a generic
     * data collection must be returned.
     *
     * @param LoadDataRequestInterface $request The request
     * @param ConfigurationInterface $output ( OPTIONAL ) Output type configuration
     * @return DataCollectionInterface
     */
    public function loadData( LoadDataRequestInterface $request, ConfigurationInterface $ouput = null);


    /**
     * Loads last value
     *
     * If $output is set the api should try to return a subclass instance
     * of BaseTypeInterface. If it not possible return the raw output in
     * form of stdClass.
     *
     * @param LoadLastValueRequestInterface $request The request
     * @param ConfigurationInterface $output ( OPTIONAL ) Output type configuration
     * @return BaseTypeInterface|\stdClass
     */
    public function loadLastValue( LoadLastValueRequestInterface $request, ConfigurationInterface $ouput = null);


    /**
     * Writes data
     *
     * @param WriteDataRequestInterface $request  The request
     */
    public function writeData( WriteDataRequestInterface $request );

    /**
     * Loads templates
     *
     * @param TemplatesRequestInterface $request ( OPTIONAL ) The request
     * @return TemplatesCollectionInterface
     */
    public function loadTemplates( TemplatesRequestInterface  $request = null);


    /**
     * Loads required fields from template
     *
     * @param RequiredFieldsRequestInterface $request The request
     * @return RequiredFieldsCollectionInterface
     */
    public function loadRequiredFields( RequiredFieldsRequestInterface  $request);

    /**
     * Logs out from LM Api
     */
    public function logout();

}
