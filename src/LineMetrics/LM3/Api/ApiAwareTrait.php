<?php
namespace LineMetrics\LM3\Api;

trait ApiAwareTrait
{

    /**
     * Api reference
     *
     * @var ApiInterface
     */
    protected $api;

    /**
     * Sets api interfrace
     *
     * @param ApiInterface $api
     * @return \LineMetrics\LM3\Api\ApiTrait
     */
    public function setApi( ApiInterface $api)
    {
        $this->api = $api;
        return $this;
    }


}