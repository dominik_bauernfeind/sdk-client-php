<?php
namespace LineMetrics\LM3\Auth;

interface AuthenticatorAwareInterface
{
    public function setAuthenticator( AuthenticatorInterface $auth);
}