<?php
namespace LineMetrics\LM3\Auth;

interface AuthTokenInterface extends \JsonSerializable, \Serializable {

    public function getAccesToken();

    public function getType();

    public function getExpires();

    public function isExpired();

}