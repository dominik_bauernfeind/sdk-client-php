<?php
namespace LineMetrics\LM3\Auth;

trait AuthenticatorAwareTrait
{
    /**
     * Authenticator instance
     *
     * @var AuthenticatorInterface
     */
    protected $authenticator;

    /**
     * Sets authenticator
     *
     * @param AuthenticatorInterface $auth Authenticator instance
     * @return \LineMetrics\LM3\Auth\AuthenticatorAwareTrait
     */
    public function setAuthenticator( AuthenticatorInterface $auth = null){
        $this->authenticator = $auth;
        return $this;
    }
}