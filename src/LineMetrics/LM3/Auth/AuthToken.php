<?php
namespace LineMetrics\LM3\Auth;

class AuthToken  implements AuthTokenInterface
{

    protected $accessToken = "";

    protected $tokenType = "";

    protected $expires = 0;

    protected $issueTime = null;

    protected $expiresTime = null;

    function __construct( $accessToken, $type, $expires, $issueTime = 'now')
    {
        $this->accessToken = $accessToken;
        $this->tokenType = $type;
        $this->expires = (int)$expires;
        $this->issueTime = new \DateTime($issueTime);
        $this->expiresTime = clone $this->issueTime;
        $this->expiresTime->modify('+ ' . $expires . ' seconds');
    }


    public function getAccesToken(){
        return $this->accessToken;
    }

    public function getType(){
        return $this->tokenType;
    }

    public function getExpires(){
        return $this->expires;
    }

    public function isExpired(){
       return !($this->expiresTime <= new \DateTime());
    }

	/* (non-PHPdoc)
     * @see JsonSerializable::jsonSerialize()
     */
    public function jsonSerialize()
    {
        return array(
            "accessToken" => $this->accessToken,
            "tokenType" => $this->tokenType,
            "expires" => $this->expires,
            "issueTime" => $this->issueTime
        );
    }

	/* (non-PHPdoc)
     * @see Serializable::serialize()
     */
    public function serialize()
    {
        return serialize($this->jsonSerialize());
    }

	/* (non-PHPdoc)
     * @see Serializable::unserialize()
     */
    public function unserialize($serialized)
    {
        $props = unserialize($serialized);
        $this->accessToken = $props["accessToken"];
        $this->tokenType= $props["tokenType"];
        $this->expires = $props["expires"];
        $this->issueTime= $props["issueTime"];

        $this->expiresTime = clone $this->issueTime;
        $this->expiresTime->modify('+ ' . $this->expires . ' seconds');
    }
}