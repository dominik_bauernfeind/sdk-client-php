<?php
namespace LineMetrics\LM3\Auth;
use LineMetrics\LM3\Exception\LM3ExceptionInterface;

interface AuthenticatorInterface{

     /**
     * Authenticates
     *
     * @return AuthTokenInterface The access token
     * @throws LM3ExceptionInterface
     */
    public function auth();

}