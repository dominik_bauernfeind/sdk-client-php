<?php
namespace LineMetrics\LM3\Auth;

use Psr\Cache\CacheItemPoolInterface;

interface AuthTokenCacheAwareInterface
{
    public function setAuthTokenCache( CacheItemPoolInterface $cache );
}
