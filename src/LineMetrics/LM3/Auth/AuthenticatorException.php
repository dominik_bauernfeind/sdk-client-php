<?php
namespace LineMetrics\LM3\Auth;

use LineMetrics\LM3\Exception\LM3ExceptionInterface;

class AuthenticatorException extends \Exception implements LM3ExceptionInterface
{


}
