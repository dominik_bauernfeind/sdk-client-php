<?php
namespace LineMetrics\LM3\Auth;

use Psr\Cache\CacheItemPoolInterface;

trait AuthTokenCacheAwareTrait
{

    protected $authTokenCache = null;

    public function setAuthTokenCache(CacheItemPoolInterface $cache)
    {
        $this->authTokenCache = $cache;
        return $this;
    }
}