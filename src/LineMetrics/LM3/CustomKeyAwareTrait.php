<?php
namespace LineMetrics\LM3;

trait CustomKeyAwareTrait
{

    protected $customKey;

   /**
    * Sets custom key
    *
    * @param string|null $customKey
    * @return \LineMetrics\LM3\CustomKeyTrait
    */
    public function setCustomKey($customKey)
    {
        $this->customKey = $customKey;
        return $this;
    }

    /**
     * Gets custom key
     *
     * @return string|null
     */
    public function getCustomKey()
    {
        return $this->customKey;
    }
}