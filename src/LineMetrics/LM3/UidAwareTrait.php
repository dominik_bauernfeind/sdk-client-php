<?php
namespace LineMetrics\LM3;

trait UidAwareTrait
{
    /**
     * UID
     *
     * @var string
     */
    protected $uid;

    /**
     * Sets uid
     *
     * @param string|null $uid
     * @return \LineMetrics\LM3\AliasTrait
     */
    public function setUid($uid)
    {
        $this->uid = $uid;
        return $this;
    }

    /**
     * Gets uid
     *
     * @return string|null
     */
    public function getUid()
    {
        return $this->uid;
    }
}
